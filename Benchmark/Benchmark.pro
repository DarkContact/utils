TEMPLATE = app
CONFIG += console
CONFIG += qt

TARGET = Benchmark

include(../Utils/UtilsStatic.pri)

MAX_WARNING = false
include(../../External/Configuration.pri)
include(../../External/Benchmark.pri)

INCLUDEPATH += $$PWD/../Main

SOURCES += main.cpp

HEADERS += \
    Experiment.h \
    StringHelper/TrimRight.h \
    StringHelper/TrimLeft.h \
    StringHelper/Trim.h \
    StringHelper/ToUpper.h \
    StringHelper/ToLower.h \
    StringHelper/ToString.h \
    StringHelper/StartWith.h \
    StringHelper/Split.h \
    StringHelper/ReplaceAll.h \
    StringHelper/Join.h \
    StringHelper/FillRight.h \
    StringHelper/FillLeft.h \
    StringHelper/EndWith.h \
    StringHelper/Contains.h \
    StringHelper/Count.h \
    StringHelper/NonCopy/TrimLeftNonCopy.h \
    StringHelper/NonCopy/TrimNonCopy.h \
    StringHelper/NonCopy/TrimRightNonCopy.h \
    StringHelper/Chop.h
