//NOTE Формат подключаемых файлов: ClassDir/Function
//NOTE --benchmark_out=result.txt --benchmark_out_format=console

//#include "Experiment.h"

//#include "StringHelper/NonCopy/TrimLeftNonCopy.h"
//#include "StringHelper/NonCopy/TrimRightNonCopy.h"
//#include "StringHelper/NonCopy/TrimNonCopy.h"

#include "StringHelper/StartWith.h"
#include "StringHelper/EndWith.h"

#include "StringHelper/Contains.h"
#include "StringHelper/ReplaceAll.h"
#include "StringHelper/Split.h"
#include "StringHelper/Join.h"
#include "StringHelper/Count.h"

#include "StringHelper/ToLower.h"
#include "StringHelper/ToUpper.h"

#include "StringHelper/TrimLeft.h"
#include "StringHelper/TrimRight.h"
#include "StringHelper/Trim.h"

#include "StringHelper/FillLeft.h"
#include "StringHelper/FillRight.h"
#include "StringHelper/Chop.h"

//#include "StringHelper/ToString.h"

BENCHMARK_MAIN();
