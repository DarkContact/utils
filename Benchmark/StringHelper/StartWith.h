#pragma once
#include <benchmark/benchmark.h>

#include "Helper/StringHelper.h"
#include <QString>

static void BM_StartWith(benchmark::State& state) {
    std::string str = "abc123def456abc";
    for (auto _ : state) {
        StringHelper::startsWith(str, "abc");
    }
}
BENCHMARK(BM_StartWith);

static void BM_StartWithQt(benchmark::State& state) {
    QLatin1String qStr("abc123def456abc");
    for (auto _ : state) {
        benchmark::DoNotOptimize(qStr.startsWith(QLatin1String("abc")));
    }
}
BENCHMARK(BM_StartWithQt);
