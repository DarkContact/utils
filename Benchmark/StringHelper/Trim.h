#pragma once
#include <benchmark/benchmark.h>

#include "Helper/StringHelper.h"
#include <QString>

static void BM_Trim(benchmark::State& state) {
    std::string str = "      abc123def456abc           ";
    for (auto _ : state) {
        StringHelper::trim(str);
    }
}
BENCHMARK(BM_Trim);

static void BM_TrimQt(benchmark::State& state) {
    QLatin1String qStr("      abc123def456abc           ");
    for (auto _ : state) {
        qStr.trimmed();
    }
}
BENCHMARK(BM_TrimQt);
