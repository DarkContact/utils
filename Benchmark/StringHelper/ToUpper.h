#pragma once
#include <benchmark/benchmark.h>

#include "Helper/StringHelper.h"
#include <QString>

static void BM_ToUpper(benchmark::State& state) {
    std::string str = "ABC123DeF456abc";
    for (auto _ : state) {
        StringHelper::toUpper(str);
    }
}
BENCHMARK(BM_ToUpper);

static void BM_ToUpperQt(benchmark::State& state) {
    QString qStr = "ABC123DeF456abc";
    for (auto _ : state) {
        qStr.toUpper();
    }
}
BENCHMARK(BM_ToUpperQt);

static void BM_ToUpperStd(benchmark::State& state) {
    std::string str = "ABC123DeF456abc";
    for (auto _ : state) {
        for (auto& ch : str) {
            ch = std::toupper(ch);
        }
    }
}
BENCHMARK(BM_ToUpperStd);
