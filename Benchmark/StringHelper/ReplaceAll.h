#pragma once
#include <benchmark/benchmark.h>

#include "Helper/StringHelper.h"
#include <QString>

static void BM_ReplaceAll(benchmark::State& state) {
    std::string str = "abc123def456abc";
    for (auto _ : state) {
        StringHelper::replaceAll(str, "abc", "xf0");
    }
}
BENCHMARK(BM_ReplaceAll);

static void BM_ReplaceAllQt(benchmark::State& state) {
    QString qStr = "abc123def456abc";
    for (auto _ : state) {
        qStr.replace(QLatin1String("abc"), QLatin1String("fx0"));
    }
}
BENCHMARK(BM_ReplaceAllQt);
