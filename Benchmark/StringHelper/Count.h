#pragma once
#include <benchmark/benchmark.h>

#include "Helper/StringHelper.h"
#include <QString>

static void BM_CountCh(benchmark::State& state) {
    std::string str = "abc123def456abc";
    for (auto _ : state) {
        StringHelper::count(str, 'b');
    }
}
BENCHMARK(BM_CountCh);

static void BM_CountChQt(benchmark::State& state) {
    QString qStr = "abc123def456abc";
    for (auto _ : state) {
        qStr.count('b');
    }
}
BENCHMARK(BM_CountChQt);

static void BM_CountSubstring(benchmark::State& state) {
    std::string str = "abc123def456abc";
    for (auto _ : state) {
        StringHelper::count(str, "abc");
    }
}
BENCHMARK(BM_CountSubstring);

static void BM_CountSubstringQt(benchmark::State& state) {
    QString qStr = "abc123def456abc";
    for (auto _ : state) {
        qStr.count(QLatin1String("abc"));
    }
}
BENCHMARK(BM_CountSubstringQt);

