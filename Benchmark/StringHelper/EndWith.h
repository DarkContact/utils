#pragma once
#include <benchmark/benchmark.h>

#include "Helper/StringHelper.h"
#include <QString>

static void BM_EndWith(benchmark::State& state) {
    std::string str = "abc123def456abc";
    for (auto _ : state) {
        StringHelper::endsWith(str, "abc");
    }
}
BENCHMARK(BM_EndWith);

static void BM_EndWithQt(benchmark::State& state) {
    QLatin1String qStr("abc123def456abc");
    for (auto _ : state) {
        benchmark::DoNotOptimize(qStr.endsWith(QLatin1String("abc")));
    }
}
BENCHMARK(BM_EndWithQt);
