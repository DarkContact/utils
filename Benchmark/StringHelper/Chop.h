#pragma once
#include <benchmark/benchmark.h>

#include "Helper/StringHelper.h"
#include <QString>

static void BM_Chop(benchmark::State& state) {
    std::string str = "abc123def456abc";
    for (auto _ : state) {
        StringHelper::chop(str, 5);
    }
}
BENCHMARK(BM_Chop);

static void BM_ChopQt(benchmark::State& state) {
    QString qStr = "abc123def456abc";
    for (auto _ : state) {
        qStr.chop(5);
    }
}
BENCHMARK(BM_ChopQt);
