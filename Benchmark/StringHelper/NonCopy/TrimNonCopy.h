#pragma once
#include <benchmark/benchmark.h>

#include "Helper/StringHelper.h"
#include <QString>

static void BM_TrimNonCopy(benchmark::State& state) {
    std::string str = "      abc123def456abc           ";
    for (auto _ : state) {
        StringHelper::NonCopy::trim(str);
    }
}
BENCHMARK(BM_TrimNonCopy);
