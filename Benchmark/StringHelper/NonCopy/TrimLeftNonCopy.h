#pragma once
#include <benchmark/benchmark.h>

#include "Helper/StringHelper.h"
#include <QString>

static void BM_TrimLeftNonCopy(benchmark::State& state) {
    std::string str = "      abc123def456abc           ";
    for (auto _ : state) {
        StringHelper::NonCopy::trimLeft(str);
    }
}
BENCHMARK(BM_TrimLeftNonCopy);
