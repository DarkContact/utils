#pragma once
#include <benchmark/benchmark.h>

#include "Helper/StringHelper.h"
#include <QString>
#include <QStringList>

static void BM_Join(benchmark::State& state) {
    std::vector<std::string> strs = {"10", "20", "30", "40", "50", "60", "70", "80", "90", "100", "110", "120", "130", "140", "150", "160", "170", "180", "190", "200"};
    for (auto _ : state) {
        StringHelper::join(strs, "-");
    }
}
BENCHMARK(BM_Join);

static void BM_JoinQt(benchmark::State& state) {
    QStringList qStrs = {"10", "20", "30", "40", "50", "60", "70", "80", "90", "100", "110", "120", "130", "140", "150", "160", "170", "180", "190", "200"};
    for (auto _ : state) {
        qStrs.join('-');
    }
}
BENCHMARK(BM_JoinQt);
