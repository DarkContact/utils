#pragma once
#include <benchmark/benchmark.h>

#include "Helper/StringHelper.h"
#include <QString>

static void BM_ToLower(benchmark::State& state) {
    std::string str = "ABC123DeF456abc";
    for (auto _ : state) {
        StringHelper::toLower(str);
    }
}
BENCHMARK(BM_ToLower);

static void BM_ToLowerQt(benchmark::State& state) {
    QString qStr = "ABC123DeF456abc";
    for (auto _ : state) {
        qStr.toLower();
    }
}
BENCHMARK(BM_ToLowerQt);

static void BM_ToLowerStd(benchmark::State& state) {
    std::string str = "ABC123DeF456abc";
    for (auto _ : state) {
        for (auto& ch : str) {
            ch = std::tolower(ch);
        }
    }
}
BENCHMARK(BM_ToLowerStd);
