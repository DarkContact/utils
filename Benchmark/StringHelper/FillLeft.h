#pragma once
#include <benchmark/benchmark.h>

#include "Helper/StringHelper.h"
#include <QString>

static void BM_FillLeft(benchmark::State& state) {
    std::string str = "abc123def456abc";
    for (auto _ : state) {
        StringHelper::fillLeft(str, 50, 'x');
    }
}
BENCHMARK(BM_FillLeft);

static void BM_FillLeftQt(benchmark::State& state) {
    QString qStr = "abc123def456abc";
    for (auto _ : state) {
        qStr.rightJustified(50, 'x');
    }
}
BENCHMARK(BM_FillLeftQt);
