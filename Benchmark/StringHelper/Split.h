#pragma once
#include <benchmark/benchmark.h>

#include "Helper/StringHelper.h"
#include <QString>
#include <QStringList>

static void BM_Split(benchmark::State& state) {
    std::string str = "10-20-30-40-50-60-70-80-90-100-110-120-130-140-150-160-170-180-190-200";
    for (auto _ : state) {
        StringHelper::split(str, "-");
    }
}
BENCHMARK(BM_Split);

static void BM_SplitQt(benchmark::State& state) {
    QString qStr = "10-20-30-40-50-60-70-80-90-100-110-120-130-140-150-160-170-180-190-200";
    for (auto _ : state) {
        qStr.split('-');
    }
}
BENCHMARK(BM_SplitQt);
