#pragma once
#include <benchmark/benchmark.h>

#include "Helper/StringHelper.h"
#include <QString>

static void BM_TrimLeft(benchmark::State& state) {
    std::string str = "      abc123def456abc           ";
    for (auto _ : state) {
        StringHelper::trimLeft(str);
    }
}
BENCHMARK(BM_TrimLeft);
