#pragma once
#include <benchmark/benchmark.h>

#include "Helper/StringHelper.h"
#include <QString>

static void BM_Contains(benchmark::State& state) {
    std::string str = "abc123def456abc";
    for (auto _ : state) {
        benchmark::DoNotOptimize(StringHelper::contains(str, "abc"));
    }
}
BENCHMARK(BM_Contains);

static void BM_ContainsQt(benchmark::State& state) {
    QString qStr = "abc123def456abc";
    for (auto _ : state) {
        qStr.contains(QLatin1String("abc"));
    }
}
BENCHMARK(BM_ContainsQt);
