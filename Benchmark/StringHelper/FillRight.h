#pragma once
#include <benchmark/benchmark.h>

#include "Helper/StringHelper.h"
#include <QString>

static void BM_FillRight(benchmark::State& state) {
    std::string str = "abc123def456abc";
    for (auto _ : state) {
        StringHelper::fillRight(str, 50, 'x');
    }
}
BENCHMARK(BM_FillRight);

static void BM_FillRightQt(benchmark::State& state) {
    QString qStr = "abc123def456abc";
    for (auto _ : state) {
        qStr.leftJustified(50, 'x');
    }
}
BENCHMARK(BM_FillRightQt);
