#pragma once
#include <benchmark/benchmark.h>

#include "Helper/StringHelper.h"
#include <QString>

//150ns
//OPT 30ns
static void BM_ToStringBool(benchmark::State& state) {
    for (auto _ : state) {
        for (int i = 0; i < 10; ++i) {
            benchmark::DoNotOptimize(StringHelper::toString(true));
            benchmark::DoNotOptimize(StringHelper::toString(false));
        }
    }
}
BENCHMARK(BM_ToStringBool);
