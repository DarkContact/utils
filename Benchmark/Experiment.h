#pragma once
#include <benchmark/benchmark.h>

#include "Helper/StringHelper.h"

static void BM_SplitNonOpt(benchmark::State& state) {
    std::string str = "10-20-30-40-50-60-70-80-90-100-110-120-130-140-150-160-170-180-190-200";
    for (auto _ : state) {
        StringHelper::split(str, "-", StringHelper::KeepEmptyParts, 0);
    }
}
BENCHMARK(BM_SplitNonOpt);

static void BM_SplitOpt(benchmark::State& state) {
    std::string str = "10-20-30-40-50-60-70-80-90-100-110-120-130-140-150-160-170-180-190-200";
    for (auto _ : state) {
        StringHelper::split(str, "-", StringHelper::KeepEmptyParts, 20);
    }
}
BENCHMARK(BM_SplitOpt);
