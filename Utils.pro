TEMPLATE = subdirs

SUBDIRS += Utils

SUBDIRS += Tests
Tests.depends = Utils

SUBDIRS += Benchmark
Benchmark.depends = Utils

SUBDIRS += Experiment
Experiment.depends = Utils

OTHER_FILES += README.md
