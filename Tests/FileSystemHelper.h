#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "Helper/FileSystemHelper.h"

using namespace testing;

TEST(FileSystemHelper, cleanPath) {
    ASSERT_EQ(FileSystemHelper::cleanPath("./local"), "local");
    ASSERT_EQ(FileSystemHelper::cleanPath("local/../bin"), "bin");
    ASSERT_EQ(FileSystemHelper::cleanPath("/local/usr/../bin"), "/local/bin");

    ASSERT_EQ(FileSystemHelper::cleanPath("C:/Data/../img"), "C:/img");
    ASSERT_EQ(FileSystemHelper::cleanPath("C:/Data/"), "C:/Data");
    ASSERT_EQ(FileSystemHelper::cleanPath("C:/"), "C:/");
}
