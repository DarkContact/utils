#pragma once
#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include <sstream>
#include <utility>

#include "Standart/CharType.h"

TEST(CharType, isSpace) {
    ASSERT_FALSE(utils::isSpace('a'));
    ASSERT_TRUE(utils::isSpace(' '));
    ASSERT_TRUE(utils::isSpace('\f'));
    ASSERT_TRUE(utils::isSpace('\n'));
    ASSERT_TRUE(utils::isSpace('\r'));
    ASSERT_TRUE(utils::isSpace('\t'));
    ASSERT_TRUE(utils::isSpace('\v'));
}

TEST(CharType, isDigit) {
    ASSERT_FALSE(utils::isDigit('a'));
    ASSERT_TRUE(utils::isDigit('0'));
    ASSERT_TRUE(utils::isDigit('1'));
    ASSERT_TRUE(utils::isDigit('2'));
    ASSERT_TRUE(utils::isDigit('3'));
    ASSERT_TRUE(utils::isDigit('4'));
    ASSERT_TRUE(utils::isDigit('5'));
    ASSERT_TRUE(utils::isDigit('6'));
    ASSERT_TRUE(utils::isDigit('7'));
    ASSERT_TRUE(utils::isDigit('8'));
    ASSERT_TRUE(utils::isDigit('9'));
}

TEST(CharType, isLower) {
    ASSERT_FALSE(utils::isLower('A'));
    ASSERT_TRUE(utils::isLower('a'));
    ASSERT_TRUE(utils::isLower('b'));
    ASSERT_TRUE(utils::isLower('c'));
    ASSERT_TRUE(utils::isLower('d'));
    ASSERT_TRUE(utils::isLower('e'));
    ASSERT_TRUE(utils::isLower('f'));
}

TEST(CharType, isUpper) {
    ASSERT_FALSE(utils::isUpper('a'));
    ASSERT_TRUE(utils::isUpper('A'));
    ASSERT_TRUE(utils::isUpper('B'));
    ASSERT_TRUE(utils::isUpper('C'));
    ASSERT_TRUE(utils::isUpper('D'));
    ASSERT_TRUE(utils::isUpper('E'));
    ASSERT_TRUE(utils::isUpper('F'));
}

TEST(CharType, isAlpha) {
    ASSERT_FALSE(utils::isAlpha('0'));
    ASSERT_TRUE(utils::isAlpha('a'));
    ASSERT_TRUE(utils::isAlpha('b'));
    ASSERT_TRUE(utils::isAlpha('c'));
    ASSERT_TRUE(utils::isAlpha('d'));
    ASSERT_TRUE(utils::isAlpha('e'));
    ASSERT_TRUE(utils::isAlpha('f'));
    ASSERT_TRUE(utils::isAlpha('A'));
    ASSERT_TRUE(utils::isAlpha('B'));
    ASSERT_TRUE(utils::isAlpha('C'));
    ASSERT_TRUE(utils::isAlpha('D'));
    ASSERT_TRUE(utils::isAlpha('E'));
    ASSERT_TRUE(utils::isAlpha('F'));
}

TEST(CharType, toLower) {
    ASSERT_EQ(utils::toLower('0'), '0');
    ASSERT_EQ(utils::toLower('A'), 'a');
    ASSERT_EQ(utils::toLower('F'), 'f');
    ASSERT_EQ(utils::toLower('G'), 'g');
    ASSERT_EQ(utils::toLower('H'), 'h');
}

TEST(CharType, toUpper) {
    ASSERT_EQ(utils::toUpper('0'), '0');
    ASSERT_EQ(utils::toUpper('a'), 'A');
    ASSERT_EQ(utils::toUpper('f'), 'F');
    ASSERT_EQ(utils::toUpper('g'), 'G');
    ASSERT_EQ(utils::toUpper('h'), 'H');
}
