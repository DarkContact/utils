#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "Utility/Date.h"

using namespace testing;

TEST(Date, Year_Month_Day) {
    Date date(2017, 10, 31);
    ASSERT_EQ(2017, date.year());
    ASSERT_EQ(10, date.month());
    ASSERT_EQ(31, date.day());

    Date date2(2050, 10, 31);
    ASSERT_EQ(2050, date2.year());
    ASSERT_EQ(10, date2.month());
    ASSERT_EQ(31, date2.day());
}

TEST(Date, IsValid) {
    ASSERT_TRUE(Date::isValid(2002, 5, 17));
    ASSERT_FALSE(Date::isValid(2002, 2, 30));
    ASSERT_TRUE(Date::isValid(2004, 2, 29));
    ASSERT_TRUE(Date::isValid(2000, 2, 29));
    ASSERT_FALSE(Date::isValid(2006, 2, 29));
    ASSERT_FALSE(Date::isValid(2100, 2, 29));
    ASSERT_TRUE(Date::isValid(1202, 6, 6));
    ASSERT_FALSE(Date::isValid(2002, 2, 30));

    ASSERT_FALSE(Date::isValid(1202, 6, 40));
    ASSERT_FALSE(Date::isValid(1202, 24, 20));
    ASSERT_FALSE(Date::isValid(1202, 24, 40));
    ASSERT_FALSE(Date::isValid(2017, 11, 31));

    ASSERT_FALSE(Date::isValid(0, 6, 6));
    ASSERT_FALSE(Date::isValid(2002, 5, -1));
}

TEST(Date, ToString) {
    Date date(2017, 10, 31);
    ASSERT_EQ("31.10.2017", date.toString("dd.MM.yyyy"));
    ASSERT_EQ("31-10-2017", date.toString("dd-MM-yyyy"));
    ASSERT_EQ("31-10-17", date.toString("dd-MM-yy"));
    ASSERT_EQ("17.10.31", date.toString("yy.MM.dd"));

    Date date2(2017, 9, 5);
    ASSERT_EQ("05.09.2017", date2.toString("dd.MM.yyyy"));
    ASSERT_EQ("5.9.17", date2.toString("d.M.yy"));

    Date date3(9, 8, 7);
    ASSERT_EQ("07.08.0009", date3.toString("dd.MM.yyyy"));
    ASSERT_EQ("07.08.09", date3.toString("dd.MM.yy"));
    ASSERT_EQ("7.8.09", date3.toString("d.M.yy"));
}

//TEST(Date, FromString) {
//    Date date = Date::fromString("05.09.2017", "dd.MM.yyyy");
//    ASSERT_EQ(5, date.day());
//    ASSERT_EQ(9, date.month());
//    ASSERT_EQ(2017, date.year());
//    ASSERT_EQ("05.09.2017", date.toString("dd.MM.yyyy"));

//    Date date2 = Date::fromString("05.2017", ".dd.yyyy");
//    ASSERT_FALSE(date2.isValid());
//    ASSERT_EQ(0, date2.day());
//    ASSERT_EQ(0, date2.month());
//    ASSERT_EQ(0, date2.year());

//    Date date3 = Date::fromString("abc.10", "abc.dd");
//    ASSERT_TRUE(date3.isValid());
//    ASSERT_EQ(10, date3.day());
//    ASSERT_EQ(1, date3.month());
//    ASSERT_EQ(1900, date3.year());

//    Date date4 = Date::fromString("abc.10", "abc.MM");
//    ASSERT_TRUE(date4.isValid());
//    ASSERT_EQ(1, date4.day());
//    ASSERT_EQ(10, date4.month());
//    ASSERT_EQ(1900, date4.year());
//}

TEST(Date, IsLeapYear) {
    ASSERT_FALSE(Date::isLeapYear(1990));
    ASSERT_FALSE(Date::isLeapYear(1991));
    ASSERT_TRUE(Date::isLeapYear(1992));
    ASSERT_FALSE(Date::isLeapYear(1993));
    ASSERT_FALSE(Date::isLeapYear(1994));
    ASSERT_FALSE(Date::isLeapYear(1995));
    ASSERT_TRUE(Date::isLeapYear(1996));
    ASSERT_FALSE(Date::isLeapYear(1997));
    ASSERT_FALSE(Date::isLeapYear(1998));
    ASSERT_FALSE(Date::isLeapYear(1999));
    ASSERT_TRUE(Date::isLeapYear(2000));
    ASSERT_FALSE(Date::isLeapYear(2001));
    ASSERT_FALSE(Date::isLeapYear(2002));
    ASSERT_FALSE(Date::isLeapYear(2003));
    ASSERT_TRUE(Date::isLeapYear(2004));
    ASSERT_FALSE(Date::isLeapYear(2005));
    ASSERT_FALSE(Date::isLeapYear(2006));
    ASSERT_FALSE(Date::isLeapYear(2007));
    ASSERT_TRUE(Date::isLeapYear(2008));
    ASSERT_FALSE(Date::isLeapYear(2009));
    ASSERT_FALSE(Date::isLeapYear(2010));
    ASSERT_FALSE(Date::isLeapYear(2011));
    ASSERT_TRUE(Date::isLeapYear(2012));

    ASSERT_TRUE(Date::isLeapYear(1600));
    ASSERT_TRUE(Date::isLeapYear(2000));
    ASSERT_FALSE(Date::isLeapYear(1700));
    ASSERT_FALSE(Date::isLeapYear(1800));
    ASSERT_FALSE(Date::isLeapYear(1900));

    ASSERT_TRUE(Date::isLeapYear(-1));
    ASSERT_FALSE(Date::isLeapYear(-2));
    ASSERT_FALSE(Date::isLeapYear(-3));
    ASSERT_FALSE(Date::isLeapYear(-4));
    ASSERT_TRUE(Date::isLeapYear(-5));
    ASSERT_TRUE(Date::isLeapYear(-9));
}

TEST(Date, Equal) {
    ASSERT_TRUE(Date(2017, 11, 8) == Date(2017, 11, 8));
    ASSERT_FALSE(Date(2017, 11, 8) == Date(2017, 11, 9));
    ASSERT_FALSE(Date(2017, 10, 9) == Date(2017, 11, 9));
    ASSERT_FALSE(Date(2016, 11, 6) == Date(2017, 11, 9));
}

TEST(Date, Less) {
    ASSERT_FALSE(Date(2017, 11, 8) < Date(2017, 11, 8));
    ASSERT_TRUE(Date(2017, 11, 8) < Date(2017, 11, 9));
    ASSERT_TRUE(Date(2017, 10, 9) < Date(2017, 11, 9));
    ASSERT_TRUE(Date(2016, 11, 9) < Date(2017, 11, 9));
    ASSERT_TRUE(Date(2016, 10, 9) < Date(2017, 11, 9));
    ASSERT_TRUE(Date(2016, 10, 8) < Date(2017, 11, 9));
}

TEST(Date, AddDays) {
    Date date(2018, 3, 23);
    ASSERT_EQ(Date(2018, 3, 23), date.addDays(0));
    ASSERT_EQ(Date(2018, 3, 25), date.addDays(2));
    ASSERT_EQ(Date(2018, 3, 21), date.addDays(-2));

    ASSERT_EQ(Date(2018, 4, 6), date.addDays(14));
    ASSERT_EQ(Date(2018, 2, 23), date.addDays(-28));

    ASSERT_EQ(Date(2019, 3, 23), date.addDays(365));
    ASSERT_EQ(Date(2017, 3, 23), date.addDays(-365));
}

TEST(Date, AddMonths) {
    Date date(2018, 3, 23);
    ASSERT_EQ(Date(2018, 3, 23), date.addMonths(0));
    ASSERT_EQ(Date(2018, 5, 23), date.addMonths(2));
    ASSERT_EQ(Date(2018, 1, 23), date.addMonths(-2));

    ASSERT_EQ(Date(2019, 5, 23), date.addMonths(14));
    ASSERT_EQ(Date(2017, 1, 23), date.addMonths(-14));

    Date date2(2020, 2, 29);
    ASSERT_EQ(Date(2021, 2, 28), date2.addMonths(12));
    ASSERT_EQ(Date(2019, 2, 28), date2.addMonths(-12));
    ASSERT_EQ(Date(2024, 2, 29), date2.addMonths(48));

    Date date3(-1, 1, 1);
    ASSERT_EQ(Date(2, 1, 1), date3.addMonths(24));
    ASSERT_EQ(Date(100, 1, 1), date3.addMonths(1200));
    ASSERT_EQ(Date(-6, 1, 1), date3.addMonths(-60));
}

TEST(Date, AddYears) {
    Date date(2018, 3, 23);
    ASSERT_EQ(Date(2020, 3, 23), date.addYears(2));
    ASSERT_EQ(Date(2016, 3, 23), date.addYears(-2));
    ASSERT_EQ(Date(2018, 3, 23), date.addYears(0));

    Date date2(2020, 2, 29);
    ASSERT_EQ(Date(2021, 2, 28), date2.addYears(1));
    ASSERT_EQ(Date(2019, 2, 28), date2.addYears(-1));
    ASSERT_EQ(Date(2024, 2, 29), date2.addYears(4));

    Date date3(-1, 1, 1);
    ASSERT_EQ(Date(2, 1, 1), date3.addYears(2));
    ASSERT_EQ(Date(100, 1, 1), date3.addYears(100));
    ASSERT_EQ(Date(-6, 1, 1), date3.addYears(-5));
}

TEST(Date, DayOfWeek) {
    Date date(2018, 3, 26);
    ASSERT_EQ(date.dayOfWeek(), 1);
    date = date.addDays(1);
    ASSERT_EQ(date.dayOfWeek(), 2);
    date = date.addDays(1);
    ASSERT_EQ(date.dayOfWeek(), 3);
    date = date.addDays(1);
    ASSERT_EQ(date.dayOfWeek(), 4);
    date = date.addDays(1);
    ASSERT_EQ(date.dayOfWeek(), 5);
    date = date.addDays(1);
    ASSERT_EQ(date.dayOfWeek(), 6);
    date = date.addDays(1);
    ASSERT_EQ(date.dayOfWeek(), 7);
    date = date.addDays(1);
    ASSERT_EQ(date.dayOfWeek(), 1);
}
