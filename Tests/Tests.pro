TEMPLATE = app
CONFIG += console thread
CONFIG -= qt

include(../Utils/UtilsStatic.pri)

include(../../External/Configuration.pri)
include(../../External/GoogleTest.pri)

INCLUDEPATH += $$PWD/../Main

HEADERS += \
    Date.h \
    DateTime.h \
    FileSystemHelper.h \
    Output.h \
    StringHelper.h \
    Time.hpp \
    CharType.h

SOURCES += main.cpp

