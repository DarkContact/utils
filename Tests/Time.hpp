#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "Utility/Time.hpp"

using namespace testing;

TEST(Time, Constructor) {
    Time time(15, 7, 10, 222);
    ASSERT_EQ(15, time.hours());
    ASSERT_EQ(7, time.minutes());
    ASSERT_EQ(10, time.seconds());
    ASSERT_EQ(222, time.milliseconds());
}

TEST(Time, ToString) {
    Time time(15, 7, 10, 2);
    ASSERT_EQ("15:07:10.002", time.toString("HH:mm:ss.zzz"));
    ASSERT_EQ("15:07:10.2", time.toString("HH:mm:ss.z"));
    ASSERT_EQ("10:7:15.002", time.toString("ss:m:HH.zzz"));
    ASSERT_EQ("15:7:10.2", time.toString("H:m:s.z"));

    Time time2(1, 2, 3, 4);
    ASSERT_EQ("01:02:03.004", time2.toString("HH:mm:ss.zzz"));
    ASSERT_EQ("1:2:3.4", time2.toString("H:m:s.z"));

    Time time3(15, 7, 10, 213);
    ASSERT_EQ("15:07:10.213", time3.toString("HH:mm:ss.zzz"));
    ASSERT_EQ("15:07:10.213", time3.toString("HH:mm:ss.z"));
}

TEST(Time, AddSecs) {
    Time time(17, 26, 27);
    ASSERT_EQ(Time(17, 26, 39), time.addSecs(12));
    ASSERT_EQ(Time(17, 27, 00), time.addSecs(33));
    ASSERT_EQ(Time(17, 28, 30), time.addSecs(123));
    ASSERT_EQ(Time(17, 24, 24), time.addSecs(-123));

    Time time2(0, 0, 0);
    ASSERT_EQ(Time(0, 0, 1), time2.addSecs(1));
    ASSERT_EQ(Time(23, 59, 59), time2.addSecs(-1));

    ASSERT_EQ(Time(0, 0, 0), time2.addSecs(86400));
    ASSERT_EQ(Time(0, 0, 1), time2.addSecs(86401));
    ASSERT_EQ(Time(0, 0, 0), time2.addSecs(-86400));
    ASSERT_EQ(Time(23, 59, 59), time2.addSecs(-86401));

    Time time3(23, 59, 59);
    ASSERT_EQ(Time(0, 0, 0), time3.addSecs(1));
    ASSERT_EQ(Time(0, 0, 1), time3.addSecs(2));
}

