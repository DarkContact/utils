#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "Helper/StringHelper.h"

using namespace testing;

TEST(StringHelper, GetString) {
    const char* constEmptyString = nullptr;
    ASSERT_TRUE(StringHelper::getString(constEmptyString).empty());

    const char* constEmptyString2 = "String";
    ASSERT_EQ("String", StringHelper::getString(constEmptyString2));

    char* emptyString = nullptr;
    ASSERT_TRUE(StringHelper::getString(emptyString).empty());

    char emptyString2[] = "String";
    ASSERT_EQ("String", StringHelper::getString(emptyString2));
}

TEST(StringHelper, StartsWith) {
    std::string testString = "01  02 03 04  05";
    ASSERT_TRUE(StringHelper::startsWith(testString, "01"));
    ASSERT_FALSE(StringHelper::startsWith(testString, "qq"));
    ASSERT_TRUE(StringHelper::startsWith(testString, "01  02 03 04  05"));
    ASSERT_TRUE(StringHelper::startsWith("Hello world!", "Hello "));
    ASSERT_FALSE(StringHelper::startsWith("Hello world!", "Hello world!Qq"));

    std::string testStringUtf8 = "いろはにほ";
    ASSERT_TRUE(StringHelper::startsWith(testStringUtf8, "いろ"));
    ASSERT_FALSE(StringHelper::startsWith(testStringUtf8, "EQ"));
    ASSERT_TRUE(StringHelper::startsWith(testStringUtf8, "いろはにほ"));
    ASSERT_TRUE(StringHelper::startsWith("Привет мир!", "Привет "));
    ASSERT_FALSE(StringHelper::startsWith("Привет мир!", "Привет мир!Qq"));
}

TEST(StringHelper, EndsWith) {
    std::string testString = "01  02 03 04  05";
    ASSERT_TRUE(StringHelper::endsWith(testString, "05"));
    ASSERT_FALSE(StringHelper::endsWith(testString, "qq"));
    ASSERT_TRUE(StringHelper::endsWith(testString, "01  02 03 04  05"));
    ASSERT_TRUE(StringHelper::endsWith("Hello world!", " world!"));
    ASSERT_FALSE(StringHelper::endsWith("Hello world!", "Hello world!qq"));

    std::string testStringUtf8 = "いろはにほ";
    ASSERT_TRUE(StringHelper::endsWith(testStringUtf8, "にほ"));
    ASSERT_FALSE(StringHelper::endsWith(testStringUtf8, "EQ"));
    ASSERT_TRUE(StringHelper::endsWith(testStringUtf8, "いろはにほ"));
    ASSERT_TRUE(StringHelper::endsWith("Привет мир!", " мир!"));
    ASSERT_FALSE(StringHelper::endsWith("Привет мир!", "Привет мир!qq"));
}

TEST(StringHelper, Contains) {
    ASSERT_TRUE(StringHelper::contains("Hello world!", "Hello world!"));
    ASSERT_TRUE(StringHelper::contains("Hello world!", "llo"));
    ASSERT_TRUE(StringHelper::contains("Hello world!", ""));

    ASSERT_TRUE(StringHelper::contains("いろはにほ", "はに"));
    ASSERT_TRUE(StringHelper::contains("Привет мир!", "вет м"));
    ASSERT_FALSE(StringHelper::contains("Привет мир!", "ветка"));
}

TEST(StringHelper, Split) {
    std::string testString = "01  02 03 04  05";
    auto subStrings = StringHelper::split(testString, " "); //Keep
    ASSERT_EQ("01", subStrings[0]);
    ASSERT_EQ("", subStrings[1]);
    ASSERT_EQ("02", subStrings[2]);
    ASSERT_EQ("03", subStrings[3]);
    ASSERT_EQ("04", subStrings[4]);
    ASSERT_EQ("", subStrings[5]);
    ASSERT_EQ("05", subStrings[6]);

    testString = "0102030405";
    subStrings = StringHelper::split(testString, "[]");
    ASSERT_EQ("0102030405", subStrings[0]);

    testString = "";
    subStrings = StringHelper::split(testString, "[]");
    ASSERT_EQ("", subStrings[0]);
}

TEST(StringHelper, Split_String) {
    std::string testString = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.";
    auto subStrings = StringHelper::split(testString, " ");
    ASSERT_EQ("Lorem", subStrings[0]);
    ASSERT_EQ("ipsum", subStrings[1]);
    ASSERT_EQ("dolor", subStrings[2]);
    ASSERT_EQ("sit", subStrings[3]);
    ASSERT_EQ("amet,", subStrings[4]);
    ASSERT_EQ("consectetur", subStrings[5]);
    ASSERT_EQ("adipiscing", subStrings[6]);
    ASSERT_EQ("elit.", subStrings[7]);

    testString = "01020[]30405";
    subStrings = StringHelper::split(testString, "[]");
    ASSERT_EQ("01020", subStrings[0]);
    ASSERT_EQ("30405", subStrings[1]);

    testString = "01[]02[]03[]04[]05";
    subStrings = StringHelper::split(testString, "[]");
    ASSERT_EQ("01", subStrings[0]);
    ASSERT_EQ("02", subStrings[1]);
    ASSERT_EQ("03", subStrings[2]);
    ASSERT_EQ("04", subStrings[3]);
    ASSERT_EQ("05", subStrings[4]);

    testString = "01'''02'''03'''04'''05";
    subStrings = StringHelper::split(testString, "'''");
    ASSERT_EQ("01", subStrings[0]);
    ASSERT_EQ("02", subStrings[1]);
    ASSERT_EQ("03", subStrings[2]);
    ASSERT_EQ("04", subStrings[3]);
    ASSERT_EQ("05", subStrings[4]);

    testString = "01[][]02[][]03[][]04[][]05";
    subStrings = StringHelper::split(testString, "[]", StringHelper::KeepEmptyParts);
    ASSERT_EQ("01", subStrings[0]);
    ASSERT_EQ("", subStrings[1]);
    ASSERT_EQ("02", subStrings[2]);
    ASSERT_EQ("", subStrings[3]);
    ASSERT_EQ("03", subStrings[4]);
    ASSERT_EQ("", subStrings[5]);
    ASSERT_EQ("04", subStrings[6]);
    ASSERT_EQ("", subStrings[7]);
    ASSERT_EQ("05", subStrings[8]);

    testString = "01[][]02[][]03[][]04[][]05";
    subStrings = StringHelper::split(testString, "[]", StringHelper::SkipEmptyParts);
    ASSERT_EQ("01", subStrings[0]);
    ASSERT_EQ("02", subStrings[1]);
    ASSERT_EQ("03", subStrings[2]);
    ASSERT_EQ("04", subStrings[3]);
    ASSERT_EQ("05", subStrings[4]);
}

TEST(StringHelper, Split_KeepOrSkip) {
    std::string testString = "01  02 03 04  05";
    auto subStrings = StringHelper::split(testString, " ", StringHelper::KeepEmptyParts);
    ASSERT_EQ("01", subStrings[0]);
    ASSERT_EQ("", subStrings[1]);
    ASSERT_EQ("02", subStrings[2]);
    ASSERT_EQ("03", subStrings[3]);
    ASSERT_EQ("04", subStrings[4]);
    ASSERT_EQ("", subStrings[5]);
    ASSERT_EQ("05", subStrings[6]);

    testString = "01  02 03 04  05";
    subStrings = StringHelper::split(testString, " ", StringHelper::SkipEmptyParts);
    ASSERT_EQ("01", subStrings[0]);
    ASSERT_EQ("02", subStrings[1]);
    ASSERT_EQ("03", subStrings[2]);
    ASSERT_EQ("04", subStrings[3]);
    ASSERT_EQ("05", subStrings[4]);
}

TEST(StringHelper, Split_EmptyStrings) {
    auto subStrings = StringHelper::split("", " ", StringHelper::KeepEmptyParts);
    ASSERT_EQ(subStrings[0], "");

    subStrings = StringHelper::split("[123]", "", StringHelper::KeepEmptyParts);
    ASSERT_EQ(subStrings[0], "");
    ASSERT_EQ(subStrings[1], "[");
    ASSERT_EQ(subStrings[2], "1");
    ASSERT_EQ(subStrings[3], "2");
    ASSERT_EQ(subStrings[4], "3");
    ASSERT_EQ(subStrings[5], "]");
    ASSERT_EQ(subStrings[6], "");

    subStrings = StringHelper::split("[123]", "", StringHelper::SkipEmptyParts);
    ASSERT_EQ(subStrings[0], "[");
    ASSERT_EQ(subStrings[1], "1");
    ASSERT_EQ(subStrings[2], "2");
    ASSERT_EQ(subStrings[3], "3");
    ASSERT_EQ(subStrings[4], "]");

    subStrings = StringHelper::split("", "", StringHelper::KeepEmptyParts);
    ASSERT_EQ(subStrings[0], "");
    ASSERT_EQ(subStrings[1], "");

    subStrings = StringHelper::split("", "", StringHelper::SkipEmptyParts);
    ASSERT_TRUE(subStrings.empty());
}

TEST(StringHelper, Join) {
    std::string testString = "01  02 03 04  05";
    auto subStrings = StringHelper::split(testString, " "); //Keep
    ASSERT_EQ("01\n\n02\n03\n04\n\n05", StringHelper::join(subStrings, "\n"));
    ASSERT_EQ("01  02 03 04  05", StringHelper::join(subStrings, " "));

    testString = "01  02 03 04  05";
    subStrings = StringHelper::split(testString, " "); //Keep
    ASSERT_EQ("01*Y**Y*02*Y*03*Y*04*Y**Y*05", StringHelper::join(subStrings, "*Y*"));
}

TEST(StringHelper, Join_FewStrings) {
    ASSERT_EQ("1*Y*2", StringHelper::join({"1", "2"}, "*Y*"));
    ASSERT_EQ("1", StringHelper::join({"1"}, "*Y*"));
}

TEST(StringHelper, Join_EmptyStrings) {
    ASSERT_EQ("12", StringHelper::join({"1", "2"}, ""));
    ASSERT_EQ("1", StringHelper::join({"1"}, ""));
    ASSERT_EQ("", StringHelper::join({}, ""));
    ASSERT_EQ("", StringHelper::join({}, "+++"));
}

TEST(StringHelper, ToLower) {
    ASSERT_EQ(StringHelper::toLower("ABCDEFGHIJKLMNOP"), "abcdefghijklmnop");
    ASSERT_EQ(StringHelper::toLower("aBcDeFgHiJkLmNoP"), "abcdefghijklmnop");
    ASSERT_EQ(StringHelper::toLower("1QWERTYUIOPASDFGHJKLZXCVBNM"), "1qwertyuiopasdfghjklzxcvbnm");
}

TEST(StringHelper, ToUpper) {
    ASSERT_EQ(StringHelper::toUpper("abcdefghijklmnop"), "ABCDEFGHIJKLMNOP");
    ASSERT_EQ(StringHelper::toUpper("aBcDeFgHiJkLmNoP"), "ABCDEFGHIJKLMNOP");
    ASSERT_EQ(StringHelper::toUpper("1qwertyuiopasdfghjklzxcvbnm"), "1QWERTYUIOPASDFGHJKLZXCVBNM");
}

TEST(StringHelper, TrimLeft) {
    //UTF-8 Unsupport

    ASSERT_EQ(StringHelper::trimLeft(" \f\n\r\t\v   abcdefghijklmnop   "), "abcdefghijklmnop   ");
    ASSERT_EQ(StringHelper::trimLeft("   abcdefghijklmnop   "), "abcdefghijklmnop   ");

    ASSERT_EQ(StringHelper::trimLeftIf("12345abcdefghijklmnop56789", "123456789"), "abcdefghijklmnop56789");
    ASSERT_EQ(StringHelper::trimLeftIf("12345abcdef123ghijklmnop56789", "123456789"), "abcdef123ghijklmnop56789");
}

TEST(StringHelper, TrimRight) {
    ASSERT_EQ(StringHelper::trimRight("   abcdefghijklmnop   \f\n\r\t\v "), "   abcdefghijklmnop");
    ASSERT_EQ(StringHelper::trimRight("   abcdefghijklmnop   "), "   abcdefghijklmnop");

    ASSERT_EQ(StringHelper::trimRightIf("12345abcdefghijklmnop56789", "123456789"), "12345abcdefghijklmnop");
    ASSERT_EQ(StringHelper::trimRightIf("12345abcdef123ghijklmnop56789", "123456789"), "12345abcdef123ghijklmnop");
}

TEST(StringHelper, Trim) {
    ASSERT_EQ(StringHelper::trim(" \f\n\r\t\v  abcdefghijklmnop   \f\n\r\t\v "), "abcdefghijklmnop");
    ASSERT_EQ(StringHelper::trim("   abcdefghijklmnop   "), "abcdefghijklmnop");
    ASSERT_EQ(StringHelper::trim("abcdefghijklmnop   "), "abcdefghijklmnop");
    ASSERT_EQ(StringHelper::trim("   abcdefghijklmnop"), "abcdefghijklmnop");

    ASSERT_EQ(StringHelper::trimIf("12345abcdefghijklmnop56789", "123456789"), "abcdefghijklmnop");
    ASSERT_EQ(StringHelper::trimIf("12345abcdef123ghijklmnop56789", "123456789"), "abcdef123ghijklmnop");
}

TEST(StringHelper, ReplaceFirst) {
    /// [replaceFirst]
    std::string testString = "abc001tyu002iyo003abc004";
    StringHelper::replaceFirst(testString, "abc", "xx");
    ASSERT_EQ(testString, "xx001tyu002iyo003abc004");

    std::string testString2 = "abc001abc002abc003abc004";
    StringHelper::replaceFirst(testString2, "abc", "");
    ASSERT_EQ(testString2, "001abc002abc003abc004");
    /// [replaceFirst]
}

TEST(StringHelper, ReplaceLast) {
    /// [replaceLast]
    std::string testString = "abc001tyu002iyo003abc004";
    StringHelper::replaceLast(testString, "abc", "xx");
    ASSERT_EQ(testString, "abc001tyu002iyo003xx004");

    std::string testString2 = "abc001abc002abc003abc004";
    StringHelper::replaceLast(testString2, "abc", "");
    ASSERT_EQ(testString2, "abc001abc002abc003004");
    /// [replaceLast]
}

TEST(StringHelper, ReplaceAll) {
    /// [replaceAll]
    std::string testString = "abc001tyu002iyo003abc004";
    StringHelper::replaceAll(testString, "abc", "xx");
    ASSERT_EQ(testString, "xx001tyu002iyo003xx004");

    std::string testString2 = "abc001abc002abc003abc004";
    StringHelper::replaceAll(testString2, "abc", "");
    ASSERT_EQ(testString2, "001002003004");
    /// [replaceAll]
}

TEST(StringHelper, ReplaceAll_Advanced) {
    std::string testString = "ababcc";
    StringHelper::replaceAll(testString, "abc", "");
    ASSERT_EQ(testString, "abc");

    testString = "01aaaaaa02";
    StringHelper::replaceAll(testString, "a", "x");
    ASSERT_EQ(testString, "01xxxxxx02");

    testString = "01aaaaaa02";
    StringHelper::replaceAll(testString, "01aaaaaa02", "");
    ASSERT_EQ(testString, "");
}

TEST(StringHelper, LeftFill) {
    ASSERT_EQ(StringHelper::fillLeft("abcd", 8, 'x'), "xxxxabcd");
    ASSERT_EQ(StringHelper::fillLeft("abcd", 8), "    abcd");

    ASSERT_EQ(StringHelper::fillLeft("abcdef", 8, 'x'), "xxabcdef");
    ASSERT_EQ(StringHelper::fillLeft("abcdef", 8, '+'), "++abcdef");
}

TEST(StringHelper, LeftFill_Truncate) {
    ASSERT_EQ(StringHelper::fillLeft("abcd", 2, 'x', StringHelper::Trunc), "ab");
    ASSERT_EQ(StringHelper::fillLeft("abcd", 4, 'x', StringHelper::Trunc), "abcd");
    ASSERT_EQ(StringHelper::fillLeft("abcd", 2, 'x', StringHelper::Keep), "abcd");
    ASSERT_EQ(StringHelper::fillLeft("abcd", 0, 'x', StringHelper::Trunc), "");
}

TEST(StringHelper, RightFill) {
    ASSERT_EQ(StringHelper::fillRight("abcd", 8, 'x'), "abcdxxxx");
    ASSERT_EQ(StringHelper::fillRight("abcd", 8), "abcd    ");

    ASSERT_EQ(StringHelper::fillRight("abcdef", 8, 'x'), "abcdefxx");
    ASSERT_EQ(StringHelper::fillRight("abcdef", 8, '+'), "abcdef++");
}

TEST(StringHelper, RightFill_Truncate) {
    ASSERT_EQ(StringHelper::fillRight("abcd", 2, 'x', StringHelper::Trunc), "ab");
    ASSERT_EQ(StringHelper::fillRight("abcd", 4, 'x', StringHelper::Trunc), "abcd");
    ASSERT_EQ(StringHelper::fillRight("abcd", 2, 'x', StringHelper::Keep), "abcd");
    ASSERT_EQ(StringHelper::fillRight("abcd", 0, 'x', StringHelper::Trunc), "");
}

TEST(StringHelper, Chop) {
    std::string string = "Hello!";
    StringHelper::chop(string, 2);
    ASSERT_EQ(string, "Hell");
    StringHelper::chop(string, 8);
    ASSERT_EQ(string, "");
}

TEST(StringHelper, Count) {
    ASSERT_EQ(StringHelper::count("10-20-30-40-50-60-70-80-90-100-110-120-130-140-150-160-170-180-190-200", "-"), 19);
    ASSERT_EQ(StringHelper::count("10--20--30", "--"), 2);
    ASSERT_EQ(StringHelper::count("10ae20ae30", "ae"), 2);

    ASSERT_EQ(StringHelper::count("10ae20ae30", ""), 0);
    ASSERT_EQ(StringHelper::count("", "ae"), 0);

    ASSERT_EQ(StringHelper::count("10-20-30-40-50-60-70-80-90-100-110-120-130-140-150-160-170-180-190-200", '-'), 19);
    ASSERT_EQ(StringHelper::count("", 'a'), 0);
}

//NOTE http://www.cl.cam.ac.uk/~mgk25/ucs/examples/UTF-8-test.txt

//$examples = array(
//    'Valid ASCII' => "a",
//    'Valid 2 Octet Sequence' => "\xc3\xb1",
//    'Invalid 2 Octet Sequence' => "\xc3\x28",
//    'Invalid Sequence Identifier' => "\xa0\xa1",
//    'Valid 3 Octet Sequence' => "\xe2\x82\xa1",
//    'Invalid 3 Octet Sequence (in 2nd Octet)' => "\xe2\x28\xa1",
//    'Invalid 3 Octet Sequence (in 3rd Octet)' => "\xe2\x82\x28",
//    'Valid 4 Octet Sequence' => "\xf0\x90\x8c\xbc",
//    'Invalid 4 Octet Sequence (in 2nd Octet)' => "\xf0\x28\x8c\xbc",
//    'Invalid 4 Octet Sequence (in 3rd Octet)' => "\xf0\x90\x28\xbc",
//    'Invalid 4 Octet Sequence (in 4th Octet)' => "\xf0\x28\x8c\x28",
//    'Valid 5 Octet Sequence (but not Unicode!)' => "\xf8\xa1\xa1\xa1\xa1",
//    'Valid 6 Octet Sequence (but not Unicode!)' => "\xfc\xa1\xa1\xa1\xa1\xa1",
//);


TEST(StringHelper_UTF8, IsValid) {
    ASSERT_TRUE(StringHelper::Utf8::isValid("a"));

    ASSERT_TRUE(StringHelper::Utf8::isValid("\xc3\xb1"));
    ASSERT_FALSE(StringHelper::Utf8::isValid("\xc3\x28"));
    ASSERT_FALSE(StringHelper::Utf8::isValid("\xa0\xa1"));

    ASSERT_TRUE(StringHelper::Utf8::isValid("\xe2\x82\xa1"));
    ASSERT_FALSE(StringHelper::Utf8::isValid("\xe2\x28\xa1"));
    ASSERT_FALSE(StringHelper::Utf8::isValid("\xe2\x82\x28"));

    ASSERT_TRUE(StringHelper::Utf8::isValid("\xf0\x90\x8c\xbc"));
    ASSERT_FALSE(StringHelper::Utf8::isValid("\xf0\x28\x8c\xbc"));
    ASSERT_FALSE(StringHelper::Utf8::isValid("\xf0\x90\x28\xbc"));
    ASSERT_FALSE(StringHelper::Utf8::isValid("\xf0\x28\x8c\x28"));
}

TEST(StringHelper_UTF8, Lenght) {
    std::string utfString = "Привет мир! ÆÉ 中文";
    ASSERT_EQ(17, StringHelper::Utf8::length(utfString));
}

TEST(StringHelper_UTF8, Chop) {
    std::string utfString = "Привет мир! ÆÉ 中文";
    StringHelper::Utf8::chop(utfString, 3);
    ASSERT_EQ("Привет мир! ÆÉ", utfString);
}

TEST(StringHelper_UTF8, ChopOverflow) {
    std::string utfString = "Привет мир! ÆÉ 中文";
    std::string utfStringCopy = utfString;
    StringHelper::Utf8::chop(utfString, 50);
    ASSERT_TRUE(utfString.empty());

    StringHelper::Utf8::chop(utfStringCopy, 17);
    ASSERT_TRUE(utfStringCopy.empty());
}

TEST(StringHelper_UTF8, ToWide) {
    ASSERT_EQ(L"Привет мир!", StringHelper::Utf8::toWide("Привет мир!"));
}

TEST(StringHelper_UTF8, ToNarrow) {
    ASSERT_EQ("Привет мир!", StringHelper::Utf8::toNarrow(L"Привет мир!"));
}



TEST(StringHelper_Convert, Int8) {
    ASSERT_EQ(18, StringHelper::toInt8("18"));
    ASSERT_EQ(-30, StringHelper::toInt8("-30"));
}

TEST(StringHelper_Convert, Int16) {
    ASSERT_EQ(31000, StringHelper::toInt16("31000"));
    ASSERT_EQ(-31000, StringHelper::toInt16("-31000"));
}

TEST(StringHelper_Convert, Int32) {
    ASSERT_EQ(666666, StringHelper::toInt32("666666"));
    ASSERT_EQ(0, StringHelper::toInt32("0"));
    ASSERT_EQ(-666666, StringHelper::toInt32("-666666"));
}

TEST(StringHelper_Convert, Int64) {
    ASSERT_EQ(66666666666666666ll, StringHelper::toInt64("66666666666666666"));
    ASSERT_EQ(-66666666666666666ll, StringHelper::toInt64("-66666666666666666"));

    std::string str = "123456789";
    ASSERT_EQ(123456789ll, StringHelper::toInt64(str));
}

TEST(StringHelper_Convert, UInt8) {
    ASSERT_EQ(240, StringHelper::toUInt8("240"));
}

TEST(StringHelper_Convert, UInt16) {
    ASSERT_EQ(65000, StringHelper::toUInt16("65000"));
}

TEST(StringHelper_Convert, UInt32) {
    ASSERT_EQ(6666666u, StringHelper::toUInt32("6666666"));
}

TEST(StringHelper_Convert, UInt64) {
    ASSERT_EQ(666666666666666666ull, StringHelper::toUInt64("666666666666666666"));
}

TEST(StringHelper_Convert, Float) {
    ASSERT_EQ(0.0001f, StringHelper::toFloat("0.0001"));
    ASSERT_EQ(0.15f, StringHelper::toFloat("0.15"));
}

TEST(StringHelper_Convert, Double) {
    ASSERT_EQ(0.0001, StringHelper::toDouble("0.0001"));
    ASSERT_EQ(0.1515, StringHelper::toDouble("0.1515"));

    bool isOk;
    ASSERT_EQ(0.0000000001, StringHelper::toDouble("0.0000000001", &isOk));
    ASSERT_TRUE(isOk);

    ASSERT_EQ(0.0, StringHelper::toDouble("0.", &isOk));
    ASSERT_TRUE(isOk);

    ASSERT_EQ(0.0, StringHelper::toDouble("100elf", &isOk));
    ASSERT_FALSE(isOk);

    ASSERT_EQ(6.02e23, StringHelper::toDouble("6.02e23"));
}

TEST(StringHelper_Convert, DoubleInfinity) {
    bool isOk;
    ASSERT_EQ(std::numeric_limits<double>::infinity(), StringHelper::toDouble("infinity", &isOk)); ASSERT_TRUE(isOk);
    ASSERT_EQ(std::numeric_limits<double>::infinity(), StringHelper::toDouble("INFINITY", &isOk)); ASSERT_TRUE(isOk);
    ASSERT_EQ(std::numeric_limits<double>::infinity(), StringHelper::toDouble("inf", &isOk)); ASSERT_TRUE(isOk);
    ASSERT_EQ(std::numeric_limits<double>::infinity(), StringHelper::toDouble("INF", &isOk)); ASSERT_TRUE(isOk);

    ASSERT_EQ(0.0, StringHelper::toDouble("abcdefg", &isOk)); ASSERT_FALSE(isOk);
    ASSERT_EQ(0.0, StringHelper::toDouble("info", &isOk)); ASSERT_FALSE(isOk);
    ASSERT_EQ(0.0, StringHelper::toDouble("infinityq", &isOk)); ASSERT_FALSE(isOk);

    ASSERT_EQ(std::numeric_limits<double>::infinity(), StringHelper::toDouble("   inf", &isOk)); ASSERT_TRUE(isOk);
    ASSERT_EQ(std::numeric_limits<double>::infinity(), StringHelper::toDouble("\tinf", &isOk)); ASSERT_TRUE(isOk);

    ASSERT_EQ(+std::numeric_limits<double>::infinity(), StringHelper::toDouble("+inf", &isOk)); ASSERT_TRUE(isOk);
    ASSERT_EQ(-std::numeric_limits<double>::infinity(), StringHelper::toDouble("-inf", &isOk)); ASSERT_TRUE(isOk);
}

TEST(StringHelper_Convert, DoubleNan) {
    //Сравнивать два nan нельзя!
    bool isOk;
    double dnan = StringHelper::toDouble("nan", &isOk); ASSERT_TRUE(isOk);
    double dNAN = StringHelper::toDouble("NAN", &isOk); ASSERT_TRUE(isOk);
    ASSERT_TRUE(std::isnan(dnan));
    ASSERT_TRUE(std::isnan(dNAN));

    dnan = StringHelper::toDouble("-nan", &isOk); ASSERT_TRUE(isOk);
    dNAN = StringHelper::toDouble("-NAN", &isOk); ASSERT_TRUE(isOk);
    ASSERT_TRUE(std::isnan(dnan));
    ASSERT_TRUE(std::isnan(dNAN));

    dnan = StringHelper::toDouble("nano", &isOk); ASSERT_FALSE(isOk);
    ASSERT_FALSE(std::isnan(dnan));

//    dnan = StringHelper::toDouble("nan(1)", &isOk); ASSERT_TRUE(isOk);
//    ASSERT_TRUE(std::isnan(dnan));
}

TEST(StringHelper_Convert, Letters) {
     ASSERT_EQ(0, StringHelper::toInt8("abc"));
     ASSERT_EQ(0, StringHelper::toInt16("abc"));
     ASSERT_EQ(0, StringHelper::toInt32("abc"));
     ASSERT_EQ(0ll, StringHelper::toInt64("abc"));

     ASSERT_EQ(0u, StringHelper::toUInt8("abc"));
     ASSERT_EQ(0u, StringHelper::toUInt16("abc"));
     ASSERT_EQ(0u, StringHelper::toUInt32("abc"));
     ASSERT_EQ(0ull, StringHelper::toUInt64("abc"));

     ASSERT_EQ(0.0f, StringHelper::toFloat("abc"));
     ASSERT_EQ(0.0, StringHelper::toDouble("abc"));
}

TEST(StringHelper_Convert, IsOk) {
    bool isOk;
    StringHelper::toInt8("123", &isOk, 10);
    ASSERT_TRUE(isOk);

    StringHelper::toInt8("abc", &isOk, 10);
    ASSERT_FALSE(isOk);

    StringHelper::toInt16("123", &isOk, 10);
    ASSERT_TRUE(isOk);

    StringHelper::toInt16("abc", &isOk, 10);
    ASSERT_FALSE(isOk);

    StringHelper::toInt32("123", &isOk, 10);
    ASSERT_TRUE(isOk);

    StringHelper::toInt32("abc", &isOk, 10);
    ASSERT_FALSE(isOk);

    StringHelper::toInt64("123", &isOk, 10);
    ASSERT_TRUE(isOk);

    StringHelper::toInt64("abc", &isOk, 10);
    ASSERT_FALSE(isOk);

    StringHelper::toUInt8("123", &isOk, 10);
    ASSERT_TRUE(isOk);

    StringHelper::toUInt8("abc", &isOk, 10);
    ASSERT_FALSE(isOk);

    StringHelper::toUInt16("123", &isOk, 10);
    ASSERT_TRUE(isOk);

    StringHelper::toUInt16("abc", &isOk, 10);
    ASSERT_FALSE(isOk);

    StringHelper::toUInt32("123", &isOk, 10);
    ASSERT_TRUE(isOk);

    StringHelper::toUInt32("abc", &isOk, 10);
    ASSERT_FALSE(isOk);

    StringHelper::toUInt64("123", &isOk, 10);
    ASSERT_TRUE(isOk);

    StringHelper::toUInt64("abc", &isOk, 10);
    ASSERT_FALSE(isOk);


    StringHelper::toInt8("0", &isOk, 10);
    ASSERT_TRUE(isOk);

    StringHelper::toInt16("0", &isOk, 10);
    ASSERT_TRUE(isOk);

    StringHelper::toInt32("0", &isOk, 10);
    ASSERT_TRUE(isOk);

    StringHelper::toInt32("000", &isOk, 10);
    ASSERT_TRUE(isOk);

    StringHelper::toInt32("   000", &isOk, 10);
    ASSERT_TRUE(isOk);

    StringHelper::toInt64("000", &isOk, 10);
    ASSERT_TRUE(isOk);

    auto value = StringHelper::toInt64("   000", &isOk, 10);
    ASSERT_TRUE(isOk);
    ASSERT_EQ(value, 0);

    auto value2 = StringHelper::toUInt32("ffzzaabb", &isOk, 16);
    ASSERT_FALSE(isOk);
    ASSERT_EQ(value2, 0u);

    auto value3 = StringHelper::toUInt16("ffzz", &isOk, 16);
    ASSERT_FALSE(isOk);
    ASSERT_EQ(value3, 0u);
}

TEST(StringHelper_Convert, IsOk_Floating) {
    bool isOk;
    StringHelper::toFloat("123", &isOk);
    ASSERT_TRUE(isOk);

    StringHelper::toFloat("abc", &isOk);
    ASSERT_FALSE(isOk);


    StringHelper::toDouble("123", &isOk);
    ASSERT_TRUE(isOk);

    StringHelper::toDouble("abc", &isOk);
    ASSERT_FALSE(isOk);


    StringHelper::toFloat("0", &isOk);
    ASSERT_TRUE(isOk);

    StringHelper::toDouble("0", &isOk);
    ASSERT_TRUE(isOk);


    StringHelper::toFloat("0.0", &isOk);
    ASSERT_TRUE(isOk);

    StringHelper::toDouble("0.0", &isOk);
    ASSERT_TRUE(isOk);


    StringHelper::toFloat("0.00", &isOk);
    ASSERT_TRUE(isOk);

    StringHelper::toDouble("0.00", &isOk);
    ASSERT_TRUE(isOk);
}

TEST(StringHelper_Convert, Overflow) {
     ASSERT_EQ(0, StringHelper::toInt8("999"));
     ASSERT_EQ(0u, StringHelper::toUInt32("9007199254740992"));

     bool isOk;
     StringHelper::toInt8("123", &isOk);
     ASSERT_TRUE(isOk);

     StringHelper::toInt8("-123", &isOk);
     ASSERT_TRUE(isOk);

     StringHelper::toInt8("-999", &isOk);
     ASSERT_FALSE(isOk);

     StringHelper::toUInt8("-123", &isOk);
     ASSERT_FALSE(isOk);

     StringHelper::toUInt8("999", &isOk);
     ASSERT_FALSE(isOk);

     auto value = StringHelper::toInt8("999", &isOk);
     ASSERT_FALSE(isOk);
     ASSERT_EQ(value, 0);

     auto value2 = StringHelper::toUInt32("9007199254740992", &isOk);
     ASSERT_FALSE(isOk);
     ASSERT_EQ(value2, 0u);
}

TEST(StringHelper_Convert, Empty) {
    ASSERT_EQ(0, StringHelper::toInt8("-"));
    ASSERT_EQ(0, StringHelper::toInt8(""));

    ASSERT_EQ(0, StringHelper::toInt32(""));
}

TEST(StringHelper_Convert, Base) {
    ASSERT_EQ(123456, StringHelper::toInt32("00011110001001000000", nullptr, 2));
    ASSERT_EQ(123456, StringHelper::toInt32("11110001001000000", nullptr, 2));

    ASSERT_EQ(123456, StringHelper::toInt32("361100", nullptr, 8));
    ASSERT_EQ(123456, StringHelper::toInt32("0361100", nullptr, 8));

    ASSERT_EQ(123456, StringHelper::toInt32("0x1E240", nullptr, 16));
    ASSERT_EQ(123456, StringHelper::toInt32("0x1e240", nullptr, 16));
    ASSERT_EQ(123456, StringHelper::toInt32("1e240", nullptr, 16));
}

//TEST(StringHelper_Convert, NoZeroTerminate) {
//    char string[] = {'1', '2', '3'};
//    ASSERT_EQ(123, StringHelper::toInt32(string));
//}

TEST(StringHelper_Convert_ToString, Bool) {
    ASSERT_EQ("true", StringHelper::toString(true));
    ASSERT_EQ("false", StringHelper::toString(false));
}

TEST(StringHelper_Convert_ToString, Int) {
    ASSERT_EQ("1", StringHelper::toString(1));
    ASSERT_EQ("0", StringHelper::toString(0));
}

TEST(StringHelper_Convert_ToString, Int32) {
    ASSERT_EQ("12345", StringHelper::toString(12345));
    ASSERT_EQ("2147483647", StringHelper::toString(INT32_MAX));
    ASSERT_EQ("-2147483648", StringHelper::toString(INT32_MIN));
    ASSERT_EQ("-6", StringHelper::toString(-6));
}

TEST(StringHelper_Convert_ToString, Int8_Bin) {
    ASSERT_EQ("10011011", StringHelper::toString(static_cast<int8_t>(-101), 2));
}

TEST(StringHelper_Convert_ToString, Int16_Bin) {
    ASSERT_EQ("1100001100111101", StringHelper::toString(static_cast<int16_t>(-15555), 2));
}

TEST(StringHelper_Convert_ToString, Int32_Bin) {
    ASSERT_EQ("11110001001000000", StringHelper::toString(123456, 2));
    ASSERT_EQ("11111111111111100001110111000000", StringHelper::toString(-123456, 2));
}

TEST(StringHelper_Convert_ToString, Int64_Bin) {
    ASSERT_EQ("1100001000000000000000000000000000000000000000000000000000000000", StringHelper::toString(-4467570830351532032ll, 2));
}

TEST(StringHelper_Convert_ToString, Int32_Hex) {
    ASSERT_EQ("3039", StringHelper::toString(12345, 16));
    ASSERT_EQ("ff", StringHelper::toString(255, 16));
    ASSERT_EQ("fffffffa", StringHelper::toString(-6, 16));
}

TEST(StringHelper_Convert_ToString, Int64) {
    ASSERT_EQ("12345", StringHelper::toString(12345ll));
    ASSERT_EQ("9223372036854775807", StringHelper::toString(INT64_MAX));
    ASSERT_EQ("-9223372036854775808", StringHelper::toString(INT64_MIN));
    ASSERT_EQ("-9223372036854775806", StringHelper::toString(-9223372036854775806ll));
}

TEST(StringHelper_Convert_ToString, Int64_Hex) {
    ASSERT_EQ("7fffffffffffffff", StringHelper::toString(INT64_MAX, 16));
    ASSERT_EQ("8000000000000000", StringHelper::toString(INT64_MIN, 16));
}

TEST(StringHelper_Convert_ToString, UInt64) {
    ASSERT_EQ("12345", StringHelper::toString(12345ull));
    ASSERT_EQ("18446744073709551615", StringHelper::toString(UINT64_MAX));
}

TEST(StringHelper_Convert_ToString, Floating) {
    ASSERT_EQ("12345.1", StringHelper::toString(12345.12312345f));
    ASSERT_EQ("12345.1", StringHelper::toString(12345.12312345));
}

TEST(StringHelper_Convert_ToString, Double) {
    ASSERT_EQ("-0.123456", StringHelper::toString(-0.123456));
    ASSERT_EQ("0.123456", StringHelper::toString(0.123456));

    //NOTE Точность 0 == 1
    ASSERT_EQ("-0.1", StringHelper::toString(-0.123456, 'g', 1));
    ASSERT_EQ("-0.09", StringHelper::toString(-0.09, 'g', 1));
    ASSERT_EQ("0.1", StringHelper::toString(0.123456, 'g', 1));

    ASSERT_EQ("-1", StringHelper::toString(-1.123456, 'g', 0));
    ASSERT_EQ("1", StringHelper::toString(1.123456, 'g', 0));
}

TEST(StringHelper_Convert_ToString, Double_NaN_Inf) {
    // NOTE в Qt выводиться nan и nan (Второй nan выводиться без знака -)
    ASSERT_EQ("NaN", StringHelper::toString(std::numeric_limits<double>::quiet_NaN()));
    ASSERT_EQ("-NaN", StringHelper::toString(-std::numeric_limits<double>::quiet_NaN()));

    ASSERT_EQ("NAN", StringHelper::toString(std::numeric_limits<double>::quiet_NaN(), 'G'));

    // NOTE в Qt выводиться inf и -inf
    ASSERT_EQ("Infinity", StringHelper::toString(std::numeric_limits<double>::infinity()));
    ASSERT_EQ("-Infinity", StringHelper::toString(-std::numeric_limits<double>::infinity()));

    ASSERT_EQ("INFINITY", StringHelper::toString(std::numeric_limits<double>::infinity(), 'E'));
    ASSERT_EQ("INFINITY", StringHelper::toString(std::numeric_limits<double>::infinity(), 'G'));
    ASSERT_EQ("INFINITY", StringHelper::toString(std::numeric_limits<double>::infinity(), 'F'));
}

TEST(StringHelper_Convert_ToString, Double_Format) {
    ASSERT_EQ("-1.000000e-101", StringHelper::toString(-0.10e-100, 'e'));
    ASSERT_EQ("1.000000e-101", StringHelper::toString(0.10e-100, 'e'));

    ASSERT_EQ("-1.00e-101", StringHelper::toString(-0.10e-100, 'e', 2));
    ASSERT_EQ("1.00e-101", StringHelper::toString(0.10e-100, 'e', 2));

    ASSERT_EQ("-1.00E-101", StringHelper::toString(-0.10e-100, 'E', 2));
    ASSERT_EQ("1.00E-101", StringHelper::toString(0.10e-100, 'E', 2));

    ASSERT_EQ("-1e-101", StringHelper::toString(-0.10e-100, 'e', 0));
    ASSERT_EQ("1e-101", StringHelper::toString(0.10e-100, 'e', 0));
}

TEST(StringHelper_Convert_ToString, Double_Rounding) {
    ASSERT_EQ("-0.123457", StringHelper::toString(-0.1234566));
    ASSERT_EQ("0.123457", StringHelper::toString(0.1234566));

    ASSERT_EQ("-0.123456", StringHelper::toString(-0.1234565));
    ASSERT_EQ("0.123456", StringHelper::toString(0.1234565));

    ASSERT_EQ("-0.123456", StringHelper::toString(-0.1234564));
    ASSERT_EQ("0.123456", StringHelper::toString(0.1234564));
}

TEST(StringHelper_Convert_ToString, Double_Precision) {
    ASSERT_EQ("0.0000000001", StringHelper::toString(0.0000000001, 'f', 10));

    ASSERT_EQ("0.123444", StringHelper::toString(0.123444, 'g', 6));
    ASSERT_EQ("0.12344", StringHelper::toString(0.123444, 'g', 5));
    ASSERT_EQ("0.1234", StringHelper::toString(0.123444, 'g', 4));
    ASSERT_EQ("0.123", StringHelper::toString(0.123444, 'g', 3));
    ASSERT_EQ("0.12", StringHelper::toString(0.123444, 'g', 2));
    ASSERT_EQ("0.1", StringHelper::toString(0.123444, 'g', 1));
    ASSERT_EQ("0.1", StringHelper::toString(0.123444, 'g', 0));

    ASSERT_EQ("-1e-101", StringHelper::toString(-0.10e-100, 'g', 2));
    ASSERT_EQ("1e-101", StringHelper::toString(0.10e-100, 'g', 2));
    ASSERT_EQ("-1E-101", StringHelper::toString(-0.10e-100, 'G', 2));
    ASSERT_EQ("1E-101", StringHelper::toString(0.10e-100, 'G', 2));

    ASSERT_EQ("-1e-101", StringHelper::toString(-0.10e-100, 'g', 0));
    ASSERT_EQ("1e-101", StringHelper::toString(0.10e-100, 'g', 0));

    // NOTE В Qt параметр когда число превращается в экспоненциальную форму = -4
    // Для double-convesion этот параметр = -4 (Возможно стоит добавить опцию для идентичности с QT)
    ASSERT_EQ("-0.01", StringHelper::toString(-0.10e-1, 'g', 2));
    ASSERT_EQ("-0.001", StringHelper::toString(-0.10e-2, 'g', 2));
    ASSERT_EQ("-0.000001", StringHelper::toString(-0.10e-5, 'g', 2));
    ASSERT_EQ("-1e-8", StringHelper::toString(-0.10e-7, 'g', 2));
}

TEST(StringHelper, Nullptr) {
    ASSERT_EQ(0.0f, StringHelper::toFloat(nullptr));
    ASSERT_EQ(0.0, StringHelper::toDouble(nullptr));
    ASSERT_EQ(0.0, StringHelper::toInt16(nullptr));
    ASSERT_EQ(0.0, StringHelper::toInt32(nullptr));

    ASSERT_TRUE(StringHelper::startsWith(nullptr, nullptr));
    ASSERT_TRUE(StringHelper::endsWith(nullptr, nullptr));
}


TEST(StringHelper, Capitalize) {
    std::string test = "heLLo World!";
    StringHelper::NonCopy::capitalize(test);
    ASSERT_EQ("Hello world!", test);

    std::string test2 = "TEST STRING!!!";
    StringHelper::NonCopy::capitalize(test2);
    ASSERT_EQ("Test string!!!", test2);

    std::string test3 = "TeSt StRiNg!!!";
    StringHelper::NonCopy::capitalize(test3);
    ASSERT_EQ("Test string!!!", test3);

    std::string test4 = "";
    StringHelper::NonCopy::capitalize(test4);
    ASSERT_EQ("", test4);
}
