#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include <sstream>
#include <utility>

#include "Utility/Output.h"

using namespace testing;

TEST(Output, Release) {
    std::ostringstream outputString;
    OUT_RELEASE().setOutput(outputString) << "123";
    ASSERT_EQ(outputString.rdbuf()->str(), "123\n");

    std::ostringstream outputString2;
    OUT_RELEASE().noColor().setOutput(outputString2, false) << "123";
    ASSERT_EQ(outputString2.rdbuf()->str(), "Utils\\Tests\\Output.h:17 | 123\n");

#ifdef NDEBUG
    std::ostringstream outputString3;
    OUT().setOutput(outputString3) << "123";
    ASSERT_EQ(outputString3.rdbuf()->str(), "");
#endif
}

#ifndef NDEBUG
TEST(Output, Default) {
    std::ostringstream outputString;
    OUT().setOutput(outputString) << "123";
    ASSERT_EQ(outputString.rdbuf()->str(), "123\n");

    std::ostringstream outputString2;
    OUT().noColor().setOutput(outputString2, false) << "123";
    ASSERT_EQ(outputString2.rdbuf()->str(), "Utils\\Tests\\Output.h:34 | 123\n");
}

TEST(Output, Color) {
    std::ostringstream outputString;
    OUT().noContext().setOutput(outputString, false) << "123";
    ASSERT_EQ(outputString.rdbuf()->str(), "\033[32m123\033[0m\n");

    std::ostringstream outputString2;
    OUT_WARN().noContext().setOutput(outputString2, false) << "123";
    ASSERT_EQ(outputString2.rdbuf()->str(), "\033[33m123\033[0m\n");

    std::ostringstream outputString3;
    OUT_CRIT().noContext().setOutput(outputString3, false) << "123";
    ASSERT_EQ(outputString3.rdbuf()->str(), "\033[1;31m123\033[0m\n");

    std::ostringstream outputString4;
    OUT_RELEASE().noContext().setOutput(outputString4, false) << "123";
    ASSERT_EQ(outputString4.rdbuf()->str(), "\033[1m123\033[0m\n");

    std::ostringstream outputString5;
    OUT().setOutput(outputString5, false) << "123";
    ASSERT_EQ(outputString5.rdbuf()->str(), "\033[37mUtils\\Tests\\Output.h:56 | \033[0m\033[32m123\033[0m\n");
}

TEST(Output, Bool) {
    std::ostringstream outputString;
    OUT().setOutput(outputString) << false;
    ASSERT_EQ(outputString.rdbuf()->str(), "false\n");

    std::ostringstream outputString2;
    OUT().setOutput(outputString2) << (1 == 1);
    ASSERT_EQ(outputString2.rdbuf()->str(), "true\n");
}

TEST(Output, Int) {
    std::ostringstream outputString;
    OUT().setOutput(outputString) << 1;
    ASSERT_EQ(outputString.rdbuf()->str(), "1\n");

    std::ostringstream outputString2;
    OUT().setOutput(outputString2) << -6;
    ASSERT_EQ(outputString2.rdbuf()->str(), "-6\n");
}

TEST(Output, Float) {
    std::ostringstream outputString;
    OUT().setOutput(outputString) << 1.5f;
    ASSERT_EQ(outputString.rdbuf()->str(), "1.5\n");

    std::ostringstream outputString2;
    OUT().setOutput(outputString2) << 1.5245;
    ASSERT_EQ(outputString2.rdbuf()->str(), "1.5245\n");

    std::ostringstream outputString3;
    OUT().setOutput(outputString3) << 2.0f;
    ASSERT_EQ(outputString3.rdbuf()->str(), "2.0\n");
}

TEST(Output, Char) {
    std::ostringstream outputString;
    OUT().setOutput(outputString) << 'a';
    ASSERT_EQ(outputString.rdbuf()->str(), "'a'\n");

    std::ostringstream outputString2;
    OUT().setOutput(outputString2) << static_cast<unsigned char>('a');
    ASSERT_EQ(outputString2.rdbuf()->str(), "97\n");

    std::ostringstream outputString3;
    OUT().setOutput(outputString3) << static_cast<signed char>('a');
    ASSERT_EQ(outputString3.rdbuf()->str(), "97\n");
}

TEST(Output, Char16) {
    //NOTE http://www.dotslashzero.net/2014/08/22/understanding-characterstring-literals-in-cc/
    std::ostringstream outputString;
    OUT().setOutput(outputString) << static_cast<char16_t>(u'Я');
    ASSERT_EQ(outputString.rdbuf()->str(), "'\\u042f'\n");

    std::ostringstream outputString2;
    OUT().setOutput(outputString2) << static_cast<char16_t>(u'貓');
    ASSERT_EQ(outputString2.rdbuf()->str(), "'\\u8c93'\n");
}

TEST(Output, Char32) {
    std::ostringstream outputString;
    OUT().setOutput(outputString) << static_cast<char32_t>(u'貓');
    ASSERT_EQ(outputString.rdbuf()->str(), "'\\u8c93'\n");

    std::ostringstream outputString2;
    OUT().setOutput(outputString2) << static_cast<char32_t>(U'🍌');
    ASSERT_EQ(outputString2.rdbuf()->str(), "'\\U0001f34c'\n");
}

TEST(Output, WChar) {
    std::ostringstream outputString;
    OUT().setOutput(outputString) << static_cast<wchar_t>(L'貓');
    ASSERT_EQ(outputString.rdbuf()->str(), "'\\u8c93'\n");
}

TEST(Output, String) {
    std::ostringstream outputString;
    OUT().setOutput(outputString) << std::string("Hello world!");
    ASSERT_EQ(outputString.rdbuf()->str(), "\"Hello world!\"\n");
}

TEST(Output, WString) {
    std::ostringstream outputString;
    OUT().setOutput(outputString) << std::wstring(L"Hello world!");
    ASSERT_EQ(outputString.rdbuf()->str(), "\"Hello world!\"\n");
}

TEST(Output, Pointer) {
    std::ostringstream outputString;
    OUT().setOutput(outputString) << reinterpret_cast<int*>(0x56ab);
    ASSERT_EQ(outputString.rdbuf()->str(), "0x56AB\n");

    std::ostringstream outputString2;
    OUT().setOutput(outputString2) << nullptr;
    ASSERT_EQ(outputString2.rdbuf()->str(), "(nullptr)\n");
}

TEST(Output, UniquePtr) {
    std::ostringstream outputString;
    OUT().setOutput(outputString) << std::make_unique<int>(0x56ab);
    ASSERT_TRUE(StringHelper::startsWith(outputString.rdbuf()->str(), "0x"));

    std::ostringstream outputString2;
    auto ptr = std::make_unique<int>(0x56);
    ptr.release();
    OUT().setOutput(outputString2) << ptr;
    ASSERT_EQ(outputString2.rdbuf()->str(), "(nullptr)\n");
}

TEST(Output, SharedPtr) {
    std::ostringstream outputString;
    OUT().setOutput(outputString) << std::make_shared<int>(0x56ab);
    ASSERT_TRUE(StringHelper::startsWith(outputString.rdbuf()->str(), "(0x")
                && StringHelper::endsWith(outputString.rdbuf()->str(), ", used: 1)\n"));

    std::ostringstream outputString2;
    auto ptr = std::make_shared<int>(0x56);
    ptr.reset();
    OUT().setOutput(outputString2) << ptr;
    ASSERT_EQ(outputString2.rdbuf()->str(), "((nullptr), used: 0)\n");
}

TEST(Output, Enum) {
    enum Letter {
        A = 0,
        B,
        C
    };

    std::ostringstream outputString;
    OUT().setOutput(outputString) << C;
    ASSERT_EQ(outputString.rdbuf()->str(), "2\n");
}

TEST(Output, EnumClass) {
    enum class LetterChar : char {
        AAA = 'A',
        BBB = 'B',
        CCC = 'C'
    };

    std::ostringstream outputString;
    OUT().setOutput(outputString) << LetterChar::CCC;
    ASSERT_EQ(outputString.rdbuf()->str(), "'C'\n");
}

TEST(Output, Stringify) {
    std::ostringstream outputString;
    OUT().setOutput(outputString) << STRINGIFY(reinterpret_cast<int*>(0x56));
    ASSERT_EQ(outputString.rdbuf()->str(), "reinterpret_cast<int*>(0x56) 0x56\n");

    std::ostringstream outputString2;
    OUT().setOutput(outputString2) << STRINGIFY(5 + 3 * 2);
    ASSERT_EQ(outputString2.rdbuf()->str(), "5 + 3 * 2 11\n");

    std::ostringstream outputString3;
    OUT().setOutput(outputString3) << STRINGIFY((5 + 3) * 2);
    ASSERT_EQ(outputString3.rdbuf()->str(), "(5 + 3) * 2 16\n");
}

TEST(Output, Bin) {
    std::ostringstream outputString;
    OUT().setOutput(outputString).bin() << static_cast<uint8_t>(6);
    ASSERT_EQ(outputString.rdbuf()->str(), "00000110\n");

    std::ostringstream outputString2;
    OUT().setOutput(outputString2).bin() << static_cast<uint16_t>(6);
    ASSERT_EQ(outputString2.rdbuf()->str(), "00000110\n");

    std::ostringstream outputString3;
    OUT().setOutput(outputString3).bin() << static_cast<uint16_t>(6655);
    ASSERT_EQ(outputString3.rdbuf()->str(), "0001100111111111\n");

    std::ostringstream outputString4;
    OUT().setOutput(outputString4).bin() << static_cast<uint32_t>(6655);
    ASSERT_EQ(outputString4.rdbuf()->str(), "0001100111111111\n");

    std::ostringstream outputString5;
    OUT().setOutput(outputString5).bin() << static_cast<uint32_t>(100669951);
    ASSERT_EQ(outputString5.rdbuf()->str(), "00000110000000000001100111111111\n");

    std::ostringstream outputString6;
    OUT().setOutput(outputString6).bin() << static_cast<uint64_t>(100669951);
    ASSERT_EQ(outputString6.rdbuf()->str(), "00000110000000000001100111111111\n");

    std::ostringstream outputString7;
    OUT().setOutput(outputString7).bin() << static_cast<uint64_t>(5764607523034234879ull);
    ASSERT_EQ(outputString7.rdbuf()->str(), "0100111111111111111111111111111111111111111111111111111111111111\n");

    std::ostringstream outputString8;
    OUT().setOutput(outputString8).bin() << static_cast<int8_t>(-6);
    ASSERT_EQ(outputString8.rdbuf()->str(), "1111111111111111111111111111111111111111111111111111111111111010\n");

    std::ostringstream outputString9;
    OUT().setOutput(outputString9).bin() << static_cast<int16_t>(-6);
    ASSERT_EQ(outputString9.rdbuf()->str(), "1111111111111111111111111111111111111111111111111111111111111010\n");

    std::ostringstream outputString10;
    OUT().setOutput(outputString10).bin() << -9223372036854775808ull;
    ASSERT_EQ(outputString10.rdbuf()->str(), "1000000000000000000000000000000000000000000000000000000000000000\n");
}

TEST(Output, Hex) {
    std::ostringstream outputString;
    OUT().setOutput(outputString).hex() << 46118400291;
    ASSERT_EQ(outputString.rdbuf()->str(), "abcdef123\n");
}

TEST(Output, HexUpper) {
    std::ostringstream outputString;
    OUT().setOutput(outputString).hexUpper() << 46118400291;
    ASSERT_EQ(outputString.rdbuf()->str(), "ABCDEF123\n");
}

TEST(Output, Pair) {
    std::ostringstream outputString;
    OUT().setOutput(outputString) << std::make_pair(5, "Five");
    ASSERT_EQ(outputString.rdbuf()->str(), "(5, Five)\n");
}

TEST(Output, Vector) {
    std::ostringstream outputString;
    std::vector<int> v = {1, 2, 3, 4, 5};
    OUT().setOutput(outputString) << v;
    ASSERT_EQ(outputString.rdbuf()->str(), "[1, 2, 3, 4, 5]\n");

    std::ostringstream outputString2;
    std::vector<int> v2 = {1, 2};
    OUT().setOutput(outputString2) << v2;
    ASSERT_EQ(outputString2.rdbuf()->str(), "[1, 2]\n");

    std::ostringstream outputString3;
    std::vector<int> v3 = {1};
    OUT().setOutput(outputString3) << v3;
    ASSERT_EQ(outputString3.rdbuf()->str(), "[1]\n");

    std::ostringstream outputString4;
    std::vector<int> v4;
    OUT().setOutput(outputString4) << v4;
    ASSERT_EQ(outputString4.rdbuf()->str(), "[]\n");
}

TEST(Output, VectorVector) {
    std::ostringstream outputString;
    std::vector<std::vector<int>> v = {{1, 2, 3, 4, 5}, {6, 7, 8, 9, 10}};
    OUT().setOutput(outputString) << v;
    ASSERT_EQ(outputString.rdbuf()->str(), "[[1, 2, 3, 4, 5], [6, 7, 8, 9, 10]]\n");
}
#endif
