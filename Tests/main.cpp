//NOTE Формат подключаемых файлов: ClassName

#include "Output.h"
#include "StringHelper.h"
#include "Date.h"
#include "Time.hpp"
#include "DateTime.h"
#include "CharType.h"
#include "FileSystemHelper.h"

#include <gtest/gtest.h>

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
