#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "Utility/DateTime.h"

using namespace testing;

TEST(DateTime, Constructor) {
    DateTime dateTime(Date(2010, 9, 8), Time(16, 40, 10));
    ASSERT_EQ(2010, dateTime.date().year());
    ASSERT_EQ(9, dateTime.date().month());
    ASSERT_EQ(8, dateTime.date().day());
    ASSERT_EQ(16, dateTime.time().hours());
    ASSERT_EQ(40, dateTime.time().minutes());
    ASSERT_EQ(10, dateTime.time().seconds());
    ASSERT_EQ(0, dateTime.time().milliseconds());
}

TEST(DateTime, ToString) {
    DateTime dateTime(Date(2010, 9, 8), Time(16, 40, 10));
    ASSERT_EQ("2010.09.08 16:40:10", dateTime.toString("yyyy.MM.dd HH:mm:ss"));
    ASSERT_EQ("2010.09.08 16:40:10.000", dateTime.toString("yyyy.MM.dd HH:mm:ss.zzz"));
    ASSERT_EQ("08.09.2010 16:40:10", dateTime.toString("dd.MM.yyyy HH:mm:ss"));

    ASSERT_EQ("25.10.2018 12.04.08.032 25 08 032", DateTime(Date(2018, 10, 25), Time(12, 4, 8, 32)).toString("dd.MM.yyyy HH.mm.ss.zzz dd ss zzz"));
}

TEST(DateTime, AddSecs) {
    DateTime dateTime(Date(2018, 03, 27), Time(0, 0, 0));
    ASSERT_EQ(DateTime(Date(2018, 03, 27), Time(0, 0, 1)), dateTime.addSecs(1));
    ASSERT_EQ(DateTime(Date(2018, 03, 26), Time(23, 59, 59)), dateTime.addSecs(-1));
    ASSERT_EQ(DateTime(Date(2018, 03, 28), Time(0, 0, 1)), dateTime.addSecs(86401));
    ASSERT_EQ(DateTime(Date(2018, 03, 25), Time(23, 59, 59)), dateTime.addSecs(-86401));
}
