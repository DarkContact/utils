TEMPLATE = lib
CONFIG += staticlib
CONFIG -= qt

TARGET = Utils
DESTDIR = $$PWD/build

include(../../External/Configuration.pri)
include(../../External/External.pri)
include(../../External/DoubleConversion.pri)

SOURCES += \
    Helper/FileSystemHelper.cpp \
    Helper/StringHelper.cpp \
    Utility/Date.cpp \
    Utility/DateTime.cpp \
    Utility/Output.cpp \
    Utility/Time.cpp

HEADERS += \
    Helper/FileSystemHelper.h \
    Helper/StringHelper.h \
    Helper/StringHelper.tpp \
    Utility/Date.h \
    Utility/DateTime.h \
    Utility/Macros.h \
    Utility/Output.h \
    Utility/Time.hpp \
    Utility/TypeTraits.h \
    Wrapper/StringView_H.h \
    Wrapper/Optional_H.h \
    Helper/Generators/XorShift128.h \
    Helper/Generators/XorShift128Plus.h \
    Utility/Random.h \
    Utility/DateAlgorithms.h \
    Standart/CharConversions.h \
    Standart/CharType.h

OTHER_FILES += changelog.txt
