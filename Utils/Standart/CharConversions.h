#pragma once
//NOTE Замена для charconv

#include <cassert>
#include <climits>

#include <limits>

#include "Utility/TypeTraits.h"
#include "Utility/Macros.h"

#include "Standart/CharType.h"

namespace utils {

    struct to_chars_result {
        char* ptr;
        int ec; // 0 - OK, 132 - OVERFLOW
    };

    namespace details {

        template <typename Int,
                  typename std::enable_if<std::is_signed<Int>::value>::type* = nullptr>
        static void calc_cutoff_cutlim(typename ::utils::MakeUnsigned<Int>::Type& cutoff, int& cutlim, int base, int neg) noexcept {
            static_assert(std::is_integral<Int>() && std::is_signed<Int>(), "Type not an signed integral!");
            using UInt = typename ::utils::MakeUnsigned<Int>::Type;

            cutoff = neg ? -std::numeric_limits<Int>::min() : std::numeric_limits<Int>::max();
            cutlim = cutoff % static_cast<UInt>(base);
            cutoff /= static_cast<UInt>(base);
        }

        template <typename UInt,
                  typename std::enable_if<!std::is_signed<UInt>::value>::type* = nullptr>
        static void calc_cutoff_cutlim(UInt& cutoff, int& cutlim, int base, int) noexcept {
            static_assert(std::is_integral<UInt>() && !std::is_signed<UInt>(), "Type not an unsigned integral!");

            cutoff = std::numeric_limits<UInt>::max() / static_cast<UInt>(base);
            cutlim = std::numeric_limits<UInt>::max() % static_cast<UInt>(base);
        }

        // Замена для strtol strtoll strtoul strtoull
        template<class Int>
        static Int strto_Int(const char *nptr, bool& isOk, int base) noexcept {
            static_assert(std::is_integral<Int>(), "Type not an integral!");
            using UInt = typename ::utils::MakeUnsigned<Int>::Type;
            const char *s = nptr;
            UInt acc;
            int c;
            UInt cutoff;
            int neg = 0, any, cutlim;
            isOk = true;

            do {
                c = *s++;
            } while (utils::isSpace(c));
            if (c == '-') {
                neg = 1;
                c = *s++;
            } else if (c == '+')
                c = *s++;
            if ((base == 0 || base == 16) &&
                c == '0' && (*s == 'x' || *s == 'X')) {
                c = s[1];
                s += 2;
                base = 16;
            }
            if (base == 0)
                base = c == '0' ? 8 : 10;

            calc_cutoff_cutlim<Int>(cutoff, cutlim, base, neg);

            for (acc = 0, any = 0;; c = *s++) {
                if (utils::isDigit(c))
                    c -= '0';
                else if (utils::isAlpha(c))
                    c -= utils::isUpper(c) ? 'A' - 10 : 'a' - 10;
                else
                    break;
                if (c >= base)
                    break;
                if (any < 0 || acc > cutoff || (acc == cutoff && c > cutlim))
                    any = -1;
                else {
                    any = 1;
                    acc *= base;
                    acc += c;
                }
            }
            if (any < 0) {
                acc = 0;
                isOk = false;
            } else if (neg)
                acc = -acc;

            const char* end = (any ? s - 1 : nptr);
            if (*end != '\0') {
                acc = 0;
                isOk = false;
            }

            return (acc);
        }

        template<class Int>
        constexpr static unsigned to_chars_len(Int value, unsigned base) noexcept {
            static_assert(std::is_integral<Int>::value, "implementation bug");
            static_assert(std::is_unsigned<Int>::value, "implementation bug");

            unsigned n = 1;
            const unsigned b2 = base * base;
            const unsigned b3 = b2 * base;
            const unsigned b4 = b3 * base;
            while (true) {
                if (value < base) return n;
                if (value < b2) return n + 1;
                if (value < b3) return n + 2;
                if (value < b4) return n + 3;
                value /= b4;
                n += 4;
            }
        }

        template<class Int>
        constexpr static unsigned to_chars_len_2(Int value) noexcept {
            static_assert(std::is_integral<Int>::value, "implementation bug");
            static_assert(std::is_unsigned<Int>::value, "implementation bug");

            constexpr size_t nBits = CHAR_BIT * sizeof(Int);

            return nBits
                    - (COUNT_LEAD_ZERO_64(value)
                       - ((CHAR_BIT * sizeof(long long)) - nBits));
        }

        template <class Int>
        static to_chars_result to_chars_2(char* first, char* last, Int value) noexcept {
            static_assert(std::is_integral<Int>::value, "implementation bug");
            static_assert(std::is_unsigned<Int>::value, "implementation bug");

            to_chars_result res;
            const unsigned len = to_chars_len_2(value);

            using uInt = typename utils::MakeUnsigned<Int>::Type;
            if (UNLIKELY(static_cast<uInt>(last - first) < len)) {
                res.ptr = last;
                res.ec = EOVERFLOW;
                return res;
            }

            static constexpr char digits2[9] = "00011011";

            static constexpr char digits3[25] = "000001010011"
                                                "100101110111";

            static constexpr char digits4[65] = "00000001001000110100010101100111"
                                                "10001001101010111100110111101111";

            unsigned pos = len - 1;
            while (value >= 0b10000) {
                auto const num = (value % 0b10000) * 4;
                value /= 0b10000;
                first[pos] = digits4[num + 3];
                first[pos - 1] = digits4[num + 2];
                first[pos - 2] = digits4[num + 1];
                first[pos - 3] = digits4[num];
                pos -= 4;
            }

            if (value >= 0b01000) {
                auto const num = value * 4;
                first[pos] = digits4[num + 3];
                first[pos - 1] = digits4[num + 2];
                first[pos - 2] = digits4[num + 1];
                first[pos - 3] = digits4[num];
            } else if (value >= 0b0100) {
                auto const num = value * 3;
                first[pos] = digits3[num + 2];
                first[pos - 1] = digits3[num + 1];
                first[pos - 2] = digits3[num];
            } else if (value >= 0b010) {
                auto const num = value * 2;
                first[pos] = digits2[num + 1];
                first[pos - 1] = digits2[num];
            } else {
                first[pos] = '0' + value;
            }

            res.ptr = first + len;
            res.ec = 0;
            return res;
        }

        template <class Int>
        static to_chars_result to_chars_8(char* first, char* last, Int value) noexcept {
            static_assert(std::is_integral<Int>::value, "implementation bug");
            static_assert(std::is_unsigned<Int>::value, "implementation bug");

            to_chars_result res;
            const unsigned len = to_chars_len(value, 8u);

            using uInt = typename utils::MakeUnsigned<Int>::Type;
            if (UNLIKELY(static_cast<uInt>(last - first) < len)) {
                res.ptr = last;
                res.ec = EOVERFLOW;
                return res;
            }

            static constexpr char digits[129] =
                    "00010203040506071011121314151617"
                    "20212223242526273031323334353637"
                    "40414243444546475051525354555657"
                    "60616263646566677071727374757677";

            unsigned pos = len - 1;
            while (value >= 0100) {
                auto const num = (value % 0100) * 2;
                value /= 0100;
                first[pos] = digits[num + 1];
                first[pos - 1] = digits[num];
                pos -= 2;
            }

            if (value >= 010) {
                auto const num = value * 2;
                first[pos] = digits[num + 1];
                first[pos - 1] = digits[num];
            } else {
                first[pos] = '0' + value;
            }

            res.ptr = first + len;
            res.ec = 0;
            return res;
        }

        template <class Int>
        static to_chars_result to_chars_10(char* first, char* last, Int value) noexcept {
            static_assert(std::is_integral<Int>::value, "implementation bug");
            static_assert(std::is_unsigned<Int>::value, "implementation bug");

            to_chars_result res;
            const unsigned len = to_chars_len(value, 10u);

            using uInt = typename utils::MakeUnsigned<Int>::Type;
            if (UNLIKELY(static_cast<uInt>(last - first) < len)) {
                res.ptr = last;
                res.ec = EOVERFLOW;
                return res;
            }

            static constexpr char digits[201] =
                    "0001020304050607080910111213141516171819"
                    "2021222324252627282930313233343536373839"
                    "4041424344454647484950515253545556575859"
                    "6061626364656667686970717273747576777879"
                    "8081828384858687888990919293949596979899";

            unsigned pos = len - 1;
            while (value >= 100) {
                auto const num = (value % 100) * 2;
                value /= 100;
                first[pos] = digits[num + 1];
                first[pos - 1] = digits[num];
                pos -= 2;
            }

            if (value >= 10) {
                auto const num = value * 2;
                first[pos] = digits[num + 1];
                first[pos - 1] = digits[num];
            } else {
                first[pos] = '0' + value;
            }

            res.ptr = first + len;
            res.ec = 0;
            return res;
        }

        template <class Int>
        static to_chars_result to_chars_16(char* first, char* last, Int value) noexcept {
            static_assert(std::is_integral<Int>::value, "implementation bug");
            static_assert(std::is_unsigned<Int>::value, "implementation bug");

            to_chars_result res;
            const unsigned len = to_chars_len(value, 0x10u);

            using uInt = typename utils::MakeUnsigned<Int>::Type;
            if (UNLIKELY(static_cast<uInt>(last - first) < len)) {
                res.ptr = last;
                res.ec = EOVERFLOW;
                return res;
            }

            static constexpr char digits[513] =
                    "000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f"
                    "202122232425262728292a2b2c2d2e2f303132333435363738393a3b3c3d3e3f"
                    "404142434445464748494a4b4c4d4e4f505152535455565758595a5b5c5d5e5f"
                    "606162636465666768696a6b6c6d6e6f707172737475767778797a7b7c7d7e7f"
                    "808182838485868788898a8b8c8d8e8f909192939495969798999a9b9c9d9e9f"
                    "a0a1a2a3a4a5a6a7a8a9aaabacadaeafb0b1b2b3b4b5b6b7b8b9babbbcbdbebf"
                    "c0c1c2c3c4c5c6c7c8c9cacbcccdcecfd0d1d2d3d4d5d6d7d8d9dadbdcdddedf"
                    "e0e1e2e3e4e5e6e7e8e9eaebecedeeeff0f1f2f3f4f5f6f7f8f9fafbfcfdfeff";

            unsigned pos = len - 1;
            while (value >= 0x100) {
                auto const num = (value % 0x100) * 2;
                value /= 0x100;
                first[pos] = digits[num + 1];
                first[pos - 1] = digits[num];
                pos -= 2;
            }

            if (value >= 0x10) {
                auto const num = value * 2;
                first[pos] = digits[num + 1];
                first[pos - 1] = digits[num];
            } else {
                first[pos] = "0123456789abcdef"[value];
            }

            res.ptr = first + len;
            res.ec = 0;
            return res;
        }

        template <class Int>
        static to_chars_result to_chars_all(char* first, char* last, Int value, int base) noexcept {
            static_assert(std::is_integral<Int>::value, "implementation bug");
            static_assert(std::is_unsigned<Int>::value, "implementation bug");

            to_chars_result res;
            const unsigned len = to_chars_len(value, base);

            using uInt = typename utils::MakeUnsigned<Int>::Type;
            if (UNLIKELY(static_cast<uInt>(last - first) < len)) {
                res.ptr = last;
                res.ec = EOVERFLOW;
                return res;
            }

            static constexpr char digits[] = "0123456789abcdefghijklmnopqrstuvwxyz";
            unsigned pos = len - 1;

            while (value >= static_cast<uInt>(base)) {
                auto const quo = value / base;
                auto const rem = value % base;
                first[pos--] = digits[rem];
                value = quo;
            }
            *first = digits[value];

            res.ptr = first + len;
            res.ec = 0;
            return res;
        }
    }

    template <typename T>
    struct StringToNumber {};

    template <>
    struct StringToNumber<int8_t> { constexpr static auto convert = details::strto_Int<long>; };
    template <>
    struct StringToNumber<int16_t> { constexpr static auto convert = details::strto_Int<long>; };
    template <>
    struct StringToNumber<int32_t> { constexpr static auto convert = details::strto_Int<long>; };
    template <>
    struct StringToNumber<int64_t> { constexpr static auto convert = details::strto_Int<long long>; };

    template <>
    struct StringToNumber<uint8_t> { constexpr static auto convert = details::strto_Int<unsigned long>; };
    template <>
    struct StringToNumber<uint16_t> { constexpr static auto convert = details::strto_Int<unsigned long>; };
    template <>
    struct StringToNumber<uint32_t> { constexpr static auto convert = details::strto_Int<unsigned long>; };
    template <>
    struct StringToNumber<uint64_t> { constexpr static auto convert = details::strto_Int<unsigned long long>; };


    template <class Int>
    to_chars_result to_chars(char* first, char* last, Int value, int base) noexcept {
        assert(base >= 2 && base <= 36);

        using uInt = typename utils::MakeUnsigned<Int>::Type;
        uInt uValue = value;

        if (value == 0 && first != last) {
            *first = '0';
            return { first + 1, 0 };
        }

        if (base == 10) {
            if (std::is_signed<Int>() && value < 0) {
                if (LIKELY(first != last)) {
                    *first++ = '-';
                }
                uValue = uInt(~value) + uInt(1);
            }
        }

        switch (base) {
            case 2: return details::to_chars_2(first, last, uValue);
            case 8: return details::to_chars_8(first, last, uValue);
            case 10: return details::to_chars_10(first, last, uValue);
            case 16: return details::to_chars_16(first, last, uValue);
            default: return details::to_chars_all(first, last, uValue, base);
        }
    }
}
