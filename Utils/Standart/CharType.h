#pragma once
//NOTE Замена для cctype

namespace utils {
    constexpr static bool isSpace(int ch) noexcept {
        return ((ch >= '\t' && ch <= '\r') || ch == ' ');
    }

    constexpr static bool isDigit(int ch) noexcept {
        return (ch >= '0' && ch <= '9');
    }

    constexpr static bool isLower(int ch) noexcept {
        return (ch >= 'a' && ch <= 'z');
    }

    constexpr static bool isUpper(int ch) noexcept {
        return (ch >= 'A' && ch <= 'Z');
    }

    constexpr static bool isAlpha(int ch) noexcept {
        return (isLower(ch) || isUpper(ch));
    }

    constexpr static int toLower(int ch) noexcept {
        return isUpper(ch) ? (ch + ('a' - 'A')) : ch;
    }

    constexpr static int toUpper(int ch) noexcept {
        return isLower(ch) ? (ch - ('a' - 'A')) : ch;
    }
}
