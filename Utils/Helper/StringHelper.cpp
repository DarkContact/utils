#include "StringHelper.h"

#include "utf8.h"
#include "double-conversion/double-conversion.h"

bool StringHelper::startsWith(StringView input, StringView starting) {
    if (starting.size() > input.size()) { return false; }

    return std::equal(starting.begin(), starting.end(), input.begin());
}

bool StringHelper::endsWith(StringView input, StringView ending) {
    if (ending.size() > input.size()) { return false; }

    return std::equal(ending.rbegin(), ending.rend(), input.rbegin());
}

bool StringHelper::replaceAll(std::string& input, StringView before, StringView after) {
    if (before.empty()) { return false; }

    size_t position = 0;
    bool replaced = false;
    while ((position = input.find(before.data(), position)) != std::string::npos) {
        input.replace(position, before.length(), after.data());
        position += after.length();
        replaced = true;
    }
    return replaced;
}

bool StringHelper::replaceFirst(std::string& input, StringView before, StringView after) {
    if (before.empty()) { return false; }

    bool replaced = false;
    size_t position = input.find(before.data());
    if (position != std::string::npos) {
        input.replace(position, before.length(), after.data());
        replaced = true;
    }
    return replaced;
}

bool StringHelper::replaceLast(std::string& input, StringView before, StringView after) {
    if (before.empty()) { return false; }

    bool replaced = false;
    size_t position = input.rfind(before.data());
    if (position != std::string::npos) {
        input.replace(position, before.length(), after.data());
        replaced = true;
    }
    return replaced;
}

std::vector<std::string> StringHelper::split(StringView input, StringView delimiter, StringHelper::SplitBehavior splitBehavior, size_t expectStrings) {
    std::vector<std::string> subStrings;

    if (delimiter.empty()) {
        subStrings.reserve(input.size() + ((splitBehavior == KeepEmptyParts) ? 2 : 0));
        if (splitBehavior == KeepEmptyParts) { subStrings.emplace_back(""); }
        for (char ch : input) {
            subStrings.emplace_back(1, ch);
        }
        if (splitBehavior == KeepEmptyParts) { subStrings.emplace_back(""); }
        return subStrings;
    }

    subStrings.reserve(expectStrings);
    size_t lastPos = 0;
    size_t length = input.length();
    while (lastPos <= length) {
        size_t currPos = input.find(delimiter, lastPos);
        if (currPos == std::string::npos) { currPos = length; }

        if (currPos != lastPos || splitBehavior == KeepEmptyParts) {
            subStrings.emplace_back(input.data() + lastPos, currPos - lastPos);
        }
        lastPos = currPos + delimiter.size();
    }

    return subStrings;
}

std::string StringHelper::join(const std::vector<std::string>& strings, StringView delimiter) {
    std::string result;
    if (!strings.empty()) {
        size_t bytes = 0;
        for (auto it = strings.cbegin(); it != strings.cend(); ++it) {
            bytes += it->size();
        }
        bytes += (delimiter.size() * (strings.size() - 1));
        result.reserve(bytes);
        for (auto it = strings.cbegin(); it != strings.cend(); ++it) {
            result += *it;
            if (std::next(it) != strings.cend()) {
                result.append(delimiter.data(), delimiter.size());
            }
        }
    }
    return result;
}

int StringHelper::count(StringView string, char ch) {
    return std::count(string.begin(), string.end(), ch);
}

int StringHelper::count(StringView string, StringView subString) {
    int count = 0;
    if (!string.empty() && !subString.empty()) {
        if (subString.size() == 1) { return StringHelper::count(string, subString[0]); }

        size_t lastPos = 0;
        while (true) {
            size_t currPos = string.find(subString, lastPos);
            if (currPos == std::string::npos) { break; }
            lastPos = currPos + subString.size();
            ++count;
        }
    }
    return count;
}

std::string StringHelper::toLower(StringView input) {
    std::string result = utils::toString(input);
    NonCopy::toLower(result);
    return result;
}

std::string StringHelper::toUpper(StringView input) {
    std::string result = utils::toString(input);
    NonCopy::toUpper(result);
    return result;
}

std::string StringHelper::trimLeft(StringView input) {
    auto begin = input.cbegin();
    auto end = input.cend();
    // skip white space from start
    while (begin < end && utils::isSpace(*begin)) {
        begin++;
    }
    return {begin, end};
}

std::string StringHelper::trimRight(StringView input) {
    auto begin = input.cbegin();
    auto end = input.cend();
    // skip white space from end
    while (begin < end && utils::isSpace(end[-1])) {
        --end;
    }
    return {begin, end};
}

std::string StringHelper::trim(StringView input) {
    auto begin = input.cbegin();
    auto end = input.cend();
    // skip white space from end
    while (begin < end && utils::isSpace(end[-1])) {
        --end;
    }
    // skip white space from start
    while (begin < end && utils::isSpace(*begin)) {
        begin++;
    }
    return {begin, end};
}

std::string StringHelper::trimLeftIf(StringView input, StringView chars) {
    std::string result = utils::toString(input);
    NonCopy::trimLeftIf(result, chars);
    return result;
}

std::string StringHelper::trimRightIf(StringView input, StringView chars) {
    std::string result = utils::toString(input);
    NonCopy::trimRightIf(result, chars);
    return result;
}

std::string StringHelper::trimIf(StringView input, StringView chars) {
    std::string result = utils::toString(input);
    NonCopy::trimIf(result, chars);
    return result;
}

std::string StringHelper::fillLeft(StringView input, size_t width, char fill, Truncate trunc) {
    std::string result = utils::toString(input);
    NonCopy::fillLeft(result, width, fill, trunc);
    return result;
}

std::string StringHelper::fillRight(StringView input, size_t width, char fill, Truncate trunc) {
    std::string result = utils::toString(input);
    NonCopy::fillRight(result, width, fill, trunc);
    return result;
}

void StringHelper::chop(std::string& input, size_t n) {
    if (n >= input.length()) {
        input.clear();
        return;
    }

    input.resize(input.length() - n);
}

std::string StringHelper::capitalize(StringView input) {
    std::string result = utils::toString(input);
    NonCopy::capitalize(result);
    return result;
}

void StringHelper::NonCopy::toLower(std::string& input) {
    std::transform(input.begin(), input.end(), input.begin(), utils::toLower);
}

void StringHelper::NonCopy::toUpper(std::string& input) {
    std::transform(input.begin(), input.end(), input.begin(), utils::toUpper);
}

void StringHelper::NonCopy::trimLeft(std::string& input) {
    input.erase(input.begin(), std::find_if(input.begin(), input.end(), [](int ch) {
        return !utils::isSpace(ch);
    }));
}

void StringHelper::NonCopy::trimRight(std::string& input) {
    input.erase(std::find_if(input.rbegin(), input.rend(), [](int ch) {
        return !utils::isSpace(ch);
    }).base(), input.end());
}

void StringHelper::NonCopy::trim(std::string& input) {
    NonCopy::trimLeft(input);
    NonCopy::trimRight(input);
}

void StringHelper::NonCopy::trimLeftIf(std::string& input, StringView chars) {
    if (chars.empty()) {
        StringHelper::NonCopy::trimLeft(input);
    } else {
        input.erase(input.begin(), std::find_if(input.begin(), input.end(), [&chars](int ch) {
            return std::find(chars.begin(), chars.end(), ch) == chars.end();
        }));
    }
}

void StringHelper::NonCopy::trimRightIf(std::string& input, StringView chars) {
    if (chars.empty()) {
        StringHelper::NonCopy::trimRight(input);
    } else {
        input.erase(std::find_if(input.rbegin(), input.rend(), [&chars](int ch) {
            return std::find(chars.begin(), chars.end(), ch) == chars.end();
        }).base(), input.end());
    }
}

void StringHelper::NonCopy::trimIf(std::string& input, StringView chars) {
    NonCopy::trimLeftIf(input, chars);
    NonCopy::trimRightIf(input, chars);
}

void StringHelper::NonCopy::fillLeft(std::string& input, size_t width, char fill, Truncate trunc) {
    size_t inputSize = input.size();
    if (width > inputSize) {
        input.insert(input.begin(), width - inputSize, fill);
    } else if (trunc == Trunc) {
        input = input.substr(0, width);
    }
}

void StringHelper::NonCopy::fillRight(std::string& input, size_t width, char fill, Truncate trunc) {
    if (width > input.size()) {
        input.resize(width, fill);
    } else if (trunc == Trunc) {
        input = input.substr(0, width);
    }
}

void StringHelper::NonCopy::capitalize(std::string& input) {
    std::transform(input.begin(), input.begin() + 1, input.begin(), utils::toUpper);
    if (input.size() > 1) {
        std::transform(input.begin() + 1, input.end(), input.begin() + 1, utils::toLower);
    }
}

bool StringHelper::Utf8::isValid(StringView input) {
    return utf8::is_valid(input.begin(), input.end());
}

int StringHelper::Utf8::length(StringView input) {
    assert(StringHelper::Utf8::isValid(input));

    return utf8::unchecked::distance(input.begin(), input.end());
}

void StringHelper::Utf8::chop(std::string& input, int n) {
    if (n >= length(input)) {
        input.clear();
        return;
    }

    auto it = input.end();
    for (int i = 0; i < n; ++i) {
        utf8::unchecked::prior(it);
        input.erase(it, input.end());
    }
}

std::wstring StringHelper::Utf8::toWide(StringView input) {
    assert(StringHelper::Utf8::isValid(input));

    std::wstring result;
    utf8::unchecked::utf8to16(input.begin(), input.end(), std::back_inserter(result));
    return result;
}

std::string StringHelper::Utf8::toNarrow(WStringView input) {
    std::string result;
    utf8::unchecked::utf16to8(input.begin(), input.end(), std::back_inserter(result));
    return result;
}

float StringHelper::toFloat(StringView input, bool* isOk) {
    float result;
    if (checkInfNan(input, isOk, result)) {
        return result;
    }

    using namespace double_conversion;
    StringToDoubleConverter converter(StringToDoubleConverter::NO_FLAGS, 0.0, 0.0, NULL, NULL);

    int processed;
    int size = static_cast<int>(input.size());
    result = converter.StringToFloat(input.data(), size, &processed);
    if (isOk) {
        *isOk = (processed == size);
    }

    return result;
}

double StringHelper::toDouble(StringView input, bool* isOk) {
    double result;
    if (checkInfNan(input, isOk, result)) {
        return result;
    }

    using namespace double_conversion;
    StringToDoubleConverter converter(StringToDoubleConverter::NO_FLAGS, 0.0, 0.0, NULL, NULL);

    int processed;
    int size = static_cast<int>(input.size());
    result = converter.StringToDouble(input.data(), size, &processed);
    if (isOk) {
        *isOk = (processed == size);
    }
    return result;
}

std::string StringHelper::toString(double value, char format, int precision) {
    if (std::signbit(value) && std::isnan(value)) {
        if (format == 'G' || format == 'F' || format == 'E') {
            return "-NAN";
        }
        return "-NaN";
    }

    using namespace double_conversion;
    const DoubleToStringConverter& converter = DoubleToStringConverter::EcmaScriptConverter();
    constexpr size_t BUFFER_SIZE = std::numeric_limits<double>::digits + std::numeric_limits<double>::max_exponent + 1;
    char buffer[BUFFER_SIZE];
    StringBuilder stringBuilder(buffer, BUFFER_SIZE);
    switch(format) {
        case 'g': case 'G': {
            if (precision <= 0) { precision = 1; }
            converter.ToPrecision(value, precision, &stringBuilder);
            break;
        }
        case 'f': case 'F': {
            converter.ToFixed(value, precision, &stringBuilder);
            break;
        }
        case 'e': case 'E': {
            converter.ToExponential(value, precision, &stringBuilder);
            break;
        }
        default: format = 'g'; break;
    }

    std::string result = stringBuilder.Finalize();
    if (format == 'g' || format == 'G') {
        NonCopy::trimRightIf(result, "0");
        if (contains(result, "e")) {
            replaceFirst(result, ".0", "");
        }
        if (endsWith(result, ".")) { result.append("0"); }
    }
    if (format == 'G' || format == 'F' || format == 'E') {
        NonCopy::toUpper(result);
    }
    return result;
}
