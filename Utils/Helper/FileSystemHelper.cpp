#include "FileSystemHelper.h"

#include <cstdio>

#include <dirent.h>
#include <sys/stat.h>

#ifdef _WIN32
 #include <windows.h>
 #include <direct.h>
#else
 #include <unistd.h>
#endif

#include "StringHelper.h"

#include "utf8.h"

std::string FileSystemHelper::currentDir() {
    std::string path;
#ifdef _WIN32
    wchar_t temp[MAX_PATH];
    if (_wgetcwd(temp, MAX_PATH)) {
        path = StringHelper::Utf8::toNarrow(temp);
        normalizeSeparators(path);
    }
#else
    char temp[PATH_MAX];
    if (::getcwd(temp, PATH_MAX)) {
        path = temp;
    }
#endif
    return path;
}

bool FileSystemHelper::makeDir(StringView path) {
#ifdef _WIN32
    return CreateDirectoryW(StringHelper::Utf8::toWide(path).c_str(), NULL);
#else
    return mkdir(path.data(),
                 //chmod 755 rwx r-x r-x
                 S_IRUSR | S_IWUSR | S_IXUSR |
                 S_IRGRP |           S_IXGRP |
                 S_IROTH |           S_IXOTH ) == 0;
#endif
}


std::vector<std::string> FileSystemHelper::entryList(StringView path, Filter filter) {
#ifdef _WIN32
    typedef _WDIR DIRECTORY;
    typedef _wdirent DIRENT;
    auto openDir = _wopendir;
    auto readDir = _wreaddir;
    auto closeDir = _wclosedir;
    auto openDirPath = [](StringView path) { return StringHelper::Utf8::toWide(path); };
    auto resultString = [](const wchar_t* name) { return StringHelper::Utf8::toNarrow(name); };
    auto lenghtString = [](const wchar_t* name) { return std::wcslen(name); };
#else
    typedef DIR DIRECTORY;
    typedef dirent DIRENT;
    auto openDir = opendir;
    auto readDir = readdir;
    auto closeDir = closedir;
    auto openDirPath = [](StringView path) { return path; };
    auto resultString = [](const char* name) { return std::string(name); };
    auto lenghtString = [](const char* name) { return std::strlen(name); };
#endif

    std::vector<std::string> result;
    DIRECTORY* dir = openDir(openDirPath(path).data());
    if (dir) {
        DIRENT* entry;
        while ((entry = readDir(dir)) != NULL) {
            if (filter == SkipDots) {
                size_t lenght = lenghtString(entry->d_name);
                if (lenght == 1 && (entry->d_name[0] == '.')) { continue; }
                if (lenght == 2 && (entry->d_name[0] == '.' && entry->d_name[1] == '.')) { continue; }
            }
            result.push_back(resultString(entry->d_name));
        }
        closeDir(dir);
    }
    return result;
}

std::vector<std::string> FileSystemHelper::drives() {
#ifdef _WIN32
    std::vector<std::string> result;
    const UINT oldErrorMode = ::SetErrorMode(SEM_FAILCRITICALERRORS | SEM_NOOPENFILEERRORBOX);
    uint32_t driveBits = static_cast<uint32_t>(GetLogicalDrives()) & 0x3ffffff;
    ::SetErrorMode(oldErrorMode);
    char driveName[] = "A:/";

    while (driveBits) {
        if (driveBits & 1) {
            result.emplace_back(driveName);
        }
        driveName[0]++;
        driveBits = driveBits >> 1;
    }
    return result;
#else
    return rootDir();
#endif
}

bool FileSystemHelper::isFile(StringView path) {
#ifdef _WIN32
    return !isDir(path);
#else
    struct stat sb;
    return stat(path.data(), &sb) && S_ISREG(sb.st_mode);
#endif
}

bool FileSystemHelper::isDir(StringView path) {
#ifdef _WIN32
    DWORD attr = GetFileAttributesW(StringHelper::Utf8::toWide(path).c_str());
    return (attr != INVALID_FILE_ATTRIBUTES) && (attr & FILE_ATTRIBUTE_DIRECTORY);
#else
    struct stat sb;
    return stat(path.data(), &sb) && S_ISDIR(sb.st_mode);
#endif
}

bool FileSystemHelper::exists(StringView path) {
#ifdef _WIN32
    return GetFileAttributesW(StringHelper::Utf8::toWide(path).c_str()) != INVALID_FILE_ATTRIBUTES;
#else
    struct stat sb;
    return stat(path.data(), &sb) == 0;
#endif
}

bool FileSystemHelper::rename(StringView oldPath, StringView newPath) {
#ifdef _WIN32
    return MoveFileW(StringHelper::Utf8::toWide(oldPath).c_str(), StringHelper::Utf8::toWide(newPath).c_str());
#else
    return std::rename(oldPath.data(), newPath.data()) == 0;
#endif
}

bool FileSystemHelper::removeFile(StringView path) {
#ifdef _WIN32
    return DeleteFileW(StringHelper::Utf8::toWide(path).c_str());
#else
    return std::remove(path.data()) == 0;
#endif
}

bool FileSystemHelper::removeDir(StringView path) {
#ifdef _WIN32
    return RemoveDirectoryW(StringHelper::Utf8::toWide(path).c_str());
#else
    return ::rmdir(path.data()) == 0;
#endif
}

std::string FileSystemHelper::tempPath() {
    std::string path;
#ifdef _WIN32
    wchar_t temp[MAX_PATH];
    const DWORD len = GetTempPathW(MAX_PATH, temp);
    if (len) { // GetTempPath() can return short names, expand
        wchar_t longTemp[MAX_PATH];
        const DWORD longLen = GetLongPathNameW(temp, longTemp, MAX_PATH);
        path = longLen && longLen < MAX_PATH ?
                    StringHelper::Utf8::toNarrow(longTemp) :
                    StringHelper::Utf8::toNarrow(temp);
    }

    if (!path.empty()) {
        StringHelper::trimRightIf(path, "\\");
        normalizeSeparators(path);
    }

    if (path.empty()) {
        path = "C:/tmp";
    } else if (path.length() >= 2 && path[1] == ':') {
        // Force uppercase drive letters
        path[0] = static_cast<char>(utils::toUpper(path[0]));
    }
#else
    path = getenv("TMPDIR");
    if (path.empty()) {
        path = "/tmp";
        //TODO CleanPath?
    }
#endif
    return path;
}

std::string FileSystemHelper::homePath() {
    std::string path;
#ifdef _WIN32
    path = getenv("USERPROFILE");
    if (path.empty()) {
        path = rootPath();
    } else {
        normalizeSeparators(path);
    }
#else
    path = getenv("HOME");
    if (path.empty()) {
        path = rootPath();
        //TODO CleanPath?
    }
#endif
    return path;
}

std::string FileSystemHelper::rootPath() {
    std::string path;
#ifdef _WIN32
    path = getenv("SystemDrive");
    if (path.empty()) {
        path = "C:";
    }
    path += '/';
#else
    path = "/";
#endif
    return path;
}

std::vector<std::string> FileSystemHelper::tempDir(Filter filter) {
    return entryList(tempPath(), filter);
}

std::vector<std::string> FileSystemHelper::homeDir(Filter filter) {
    return entryList(homePath(), filter);
}

std::vector<std::string> FileSystemHelper::rootDir() {
    return entryList(rootPath(), All);
}

std::string FileSystemHelper::cleanPath(StringView path) {
    // Returns path with directory separators normalized (converted to "/") and
    // redundant ones removed, and "."s and ".."s resolved (as far as possible).
    if (path.empty()) {
        return std::string(path.data(), path.size());
    }

    std::string result(path.data(), path.size());
#ifdef _WIN32
    normalizeSeparators(result);
#endif
    result = normalizePathSegments(result);

    // Strip away last slash except for root directories
    if (result.length() > 1 && StringHelper::endsWith(result, "/")) {
        if (!(result.length() == 3 && result[1] == ':')) {
            StringHelper::chop(result, 1);
        }
    }
    return result;
}

uint64_t FileSystemHelper::fileSize(StringView path) {
#ifdef _WIN32
    ULARGE_INTEGER fileSize = { };
    WIN32_FILE_ATTRIBUTE_DATA fileData;
    if (GetFileAttributesEx(StringHelper::Utf8::toWide(path).c_str(), GetFileExInfoStandard, &fileData)) {
        fileSize.LowPart = fileData.nFileSizeLow;
        fileSize.HighPart = fileData.nFileSizeHigh;
    }
    return fileSize.QuadPart;
#else
    struct stat sb;
    if (stat(path.data(), &sb) != 0) {
        return 0;
    } else {
        return static_cast<uint64_t>(sb.st_size);
    }
#endif
}


void FileSystemHelper::normalizeSeparators(std::string& path) {
    StringHelper::replaceAll(path, "\\", "/");
}

std::string FileSystemHelper::normalizePathSegments(StringView name) {
    const size_t len = name.length();

    int i = static_cast<int>(len) - 1;
    std::vector<char16_t> outVector(len);
    size_t used = len;
    char16_t* out = outVector.data();

    std::u16string wideString;
    utf8::unchecked::utf8to16(name.begin(), name.end(), std::back_inserter(wideString));
    const char16_t *p = wideString.data();
    const char16_t *prefix = p;
    int up = 0;

    const int prefixLength = static_cast<int>(rootLength(name));
    p += prefixLength;
    i -= prefixLength;

    // replicate trailing slash (i > 0 checks for emptiness of input string p)
    if (i > 0 && p[i] == '/') {
        out[--used] = '/';
        --i;
    }

    while (i >= 0) {
        // remove trailing slashes
        if (p[i] == '/') {
            --i;
            continue;
        }

        // remove current directory
        if (p[i] == '.' && (i == 0 || p[i-1] == '/')) {
            --i;
            continue;
        }

        // detect up dir
        if (i >= 1 && p[i] == '.' && p[i-1] == '.'
                && (i == 1 || (i >= 2 && p[i-2] == '/'))) {
            ++up;
            i -= 2;
            continue;
        }

        // prepend a slash before copying when not empty
        if (!up && used != len && out[used] != '/')
            out[--used] = '/';

        // skip or copy
        while (i >= 0) {
            if (p[i] == '/') { // do not copy slashes
                --i;
                break;
            }

            // actual copy
            if (!up)
                out[--used] = p[i];
            --i;
        }

        // decrement up after copying/skipping
        if (up)
            --up;
    }

    // add remaining '..'
    while (up) {
        if (used != len && out[used] != '/') // is not empty and there isn't already a '/'
            out[--used] = '/';
        out[--used] = '.';
        out[--used] = '.';
        --up;
    }

    bool isEmpty = used == len;

    if (prefixLength) {
        if (!isEmpty && out[used] == '/') {
            // Eventhough there is a prefix the out string is a slash. This happens, if the input
            // string only consists of a prefix followed by one or more slashes. Just skip the slash.
            ++used;
        }
        for (int i = prefixLength - 1; i >= 0; --i)
            out[--used] = prefix[i];
    } else {
        if (isEmpty) {
            // After resolving the input path, the resulting string is empty (e.g. "foo/.."). Return
            // a dot in that case.
            out[--used] = '.';
        } else if (out[used] == '/') {
            // After parsing the input string, out only contains a slash. That happens whenever all
            // parts are resolved and there is a trailing slash ("./" or "foo/../" for example).
            // Prepend a dot to have the correct return value.
            out[--used] = '.';
        }
    }

    // If path was not modified return the original value
    if (used == 0) {
        return std::string(name.data(), name.size());
    }
    auto begin = out + used;
    auto end = begin + (len - used);
    std::string result;
    utf8::unchecked::utf16to8(begin, end, std::back_inserter(result));
    return result;
}

size_t FileSystemHelper::rootLength(StringView name) {
    const size_t len = name.length();
    bool allowUncPaths = false;
#ifdef _WIN32
    allowUncPaths = true;
#endif
    // starts with double slash
    if (allowUncPaths && StringHelper::startsWith(name, "//")) {
        // Server name '//server/path' is part of the prefix.
        const size_t nextSlash = name.find('/', 2);
        return nextSlash != std::string::npos ? nextSlash + 1 : len;
    }
#ifdef _WIN32
    if (len >= 2 && name[1] == ':') {
        // Handle a possible drive letter
        return len > 2 && name[2] == '/' ? 3 : 2;
    }
#endif
    if (name[0] == '/') {
        return 1;
    }
    return 0;
}
