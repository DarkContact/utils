#ifndef STRING_HELPER_H
 #include "StringHelper.h"
#endif

template<class Int, typename std::enable_if<std::is_integral<Int>::value>::type*>
std::string StringHelper::toString(Int value, int base) {
    char buffer[64]; //uint64_t && base == 2
    auto result = utils::to_chars(buffer, buffer + 64, value, base);
    return { buffer, result.ptr };
}

template<class Int>
Int StringHelper::toIntNarrow(StringView input, bool* isOk, int base) {
    static_assert(std::is_integral<Int>(), "Type not an integral!");
    assert((base >= 2 && base <= 36) || (base == 0));

    constexpr size_t maxLenght = std::numeric_limits<uint16_t>::digits;
    if (input.empty() || input.size() > maxLenght) {
        if (isOk) { *isOk = false; }
        return 0;
    }

    bool isValid;
    auto result = utils::StringToNumber<Int>::convert(input.data(), isValid, base);
    bool inRange = (result >= std::numeric_limits<Int>::min() && result <= std::numeric_limits<Int>::max());
    if (isOk) { *isOk = isValid && inRange; }
    if (!inRange) { result = 0; }
    return static_cast<Int>(result);
}

template<class Int>
Int StringHelper::toNumber(StringView input, bool* isOk, int base) {
    static_assert(std::is_integral<Int>(), "Type not an integral!");
    assert((base >= 2 && base <= 36) || (base == 0));

    constexpr size_t maxLenght = std::numeric_limits<uint64_t>::digits;
    if (input.empty() || input.size() > maxLenght) {
        if (isOk) { *isOk = false; }
        return 0;
    }

    bool isValid;
    auto result = utils::StringToNumber<Int>::convert(input.data(), isValid, base);
    if (isOk) { *isOk = isValid; }
    return result;
}

template<class Float>
bool StringHelper::checkInfNan(StringView input, bool* isOk, Float& result) {
    if (!input.empty()) {
        const char* p = input.data();

        while (utils::isSpace(*p)) { ++p; }

        bool isNegative = false;
        if (*p == '+') { ++p; }
        if (*p == '-') { ++p; isNegative = true; }

        if ((p[0] == 'i' || p[0] == 'I')
            && (p[1] == 'n' || p[1] == 'N')
            && (p[2] == 'f' || p[2] == 'F'))
        {
            if ((p[3] == 'i' || p[3] == 'I')
                && (p[4] == 'n' || p[4] == 'N')
                && (p[5] == 'i' || p[5] == 'I')
                && (p[6] == 't' || p[6] == 'T')
                && (p[7] == 'y' || p[7] == 'Y'))
            {
                if (p[8] == '\0') {
                    if (isOk) { *isOk = true; }
                    isNegative ? (result = -std::numeric_limits<Float>::infinity())
                               : (result = std::numeric_limits<Float>::infinity());
                    return true;
                }
            }
            else
            {
                if (p[3] == '\0') {
                    if (isOk) { *isOk = true; }
                    isNegative ? (result = -std::numeric_limits<Float>::infinity())
                               : (result = std::numeric_limits<Float>::infinity());
                    return true;
                }
            }
        }

        if ((p[0] == 'n' || p[0] == 'N')
            && (p[1] == 'a' || p[1] == 'A')
            && (p[2] == 'n' || p[2] == 'N'))
        {
            if (p[3] == '\0') {
                if (isOk) { *isOk = true; }
                isNegative ? (result = -std::numeric_limits<Float>::quiet_NaN())
                           : (result = std::numeric_limits<Float>::quiet_NaN());
                return true;
            }
        }
    }
    return false;
}
