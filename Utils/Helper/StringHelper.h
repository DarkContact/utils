#pragma once
#define STRING_HELPER_H

#include <cstdint>
#include <cstdio>
#include <cstring>
#include <cerrno>
#include <cassert>
#include <cmath>

#include <string>
#include <vector>
#include <limits>
#include <algorithm>

#include "Wrapper/StringView_H.h"
#include "Utility/TypeTraits.h"

#include "Standart/CharConversions.h"
#include "Standart/CharType.h"

/*!
 * \file StringHelper.h
 * \brief Функции для работы со строками
 */

//TODO base == 16 для чисел с плавающей точкой в обе стороны

/*!
 * \brief Функции для работы со строками
 */
class StringHelper {
public:
    StringHelper() = delete;

    /*!
     * \~russian
     * \brief Безопасно создаёт строку C++ из C-style строки
     * \param chars указатель на C-style строку
     * \return Cтроку C++
     *
     * \~english
     * \brief Safety create C++ string from C-style string
     * \param chars pointer to C-style string
     * \return C++ string
     */
    static std::string getString(const char* chars) { return chars ? std::string(chars) : ""; }
    static std::string getString(char* chars) { return getString(const_cast<const char*>(chars)); } ///< \copydoc getString()

    /*!
     * \brief Проверяет начинается ли строка с указаной подстроки
     * \param input проверяемая строка
     * \param starting подстрока проверки
     * \return \c true, если строка \a input, начинается со строки \a starting, иначе \c false
     *
     * \see endsWith()
     */
    static bool startsWith(StringView input, StringView starting);

    /*!
     * \brief Проверяет заканчивается ли строка указаной подстрокой
     * \param input проверяемая строка
     * \param ending подстрока проверки
     * \return \c true, если строка \a input, заканчивается строкой \a ending, иначе \c false
     *
     * \see startsWith()
     */
    static bool endsWith(StringView input, StringView ending);

    /*!
     * \brief Проверяет содержится ли подстрока в строке
     * \param input проверяемая строка
     * \param contain подстрока проверки
     * \return \c true, если строка \a input, содержит подстроку \a contain, иначе \c false
     */
    static bool contains(StringView input, StringView contain) { return (input.find(contain) != std::string::npos); }

    /*!
     * \class hide_Replace
     * \param input Строка в которой происходит замена
     * \param before Подстрока которую нужно изменить
     * \param after Подстрока на которую происходит замена
     * \return \c true, если выполнена замена, иначе \c false
     */

    /*!
     * \brief Заменяет подстроку в строке input
     * \copydoc hide_Replace
     *
     * \par Пример
     * \snippet StringHelperTests.h replaceAll
     *
     * \see replaceFirst(), replaceLast()
     */
    static bool replaceAll(std::string& input, StringView before, StringView after);

    /*!
     * \brief Заменяет первое вхождение подстроки в строке input
     * \copydoc hide_Replace
     *
     * \par Пример
     * \snippet StringHelperTests.h replaceFirst
     *
     * \see replaceAll(), replaceLast()
     */
    static bool replaceFirst(std::string& input, StringView before, StringView after);

    /*!
     * \brief Заменяет последнее вхождение подстроки в строке input
     * \copydoc hide_Replace
     *
     * \par Пример
     * \snippet StringHelperTests.h replaceLast
     *
     * \see replaceAll(), replaceFirst()
     */
    static bool replaceLast(std::string& input, StringView before, StringView after);

    /// Поведение при разбиении на подстроки
    enum SplitBehavior {
        KeepEmptyParts = 0, ///< Сохранять пустые строки
        SkipEmptyParts = 1  ///< Пропускать пустые строки
    };

    /*!
     * \brief Разбивает строку на коллекцию подстрок
     * \param input Входная строка
     * \param delimiter Разделитель
     * \param splitBehavior Поведение при разбиении на подстроки
     * \param expectStrings Ожидаемое кол-во строк после разбиения (для оптимизации)
     * \return Коллекцию подстрок
     *
     * \see join()
     */
    static std::vector<std::string> split(StringView input, StringView delimiter, SplitBehavior splitBehavior = KeepEmptyParts, size_t expectStrings = 8);

    /*!
     * \brief Объединяет коллекции подстрок в строку
     * \param strings Коллекция подстрок
     * \param delimiter Разделитель
     * \return Строку C++
     *
     * \see split()
     */
    static std::string join(const std::vector<std::string>& strings, StringView delimiter);

    /*!
     * \brief Подсчитывает кол-во вхождений в строку
     * \param string Исходная строка
     * \param ch Символ входящий в строку string
     * \return Кол-во вхождений в строку
     */
    static int count(StringView string, char ch);

    /*!
     * \brief Подсчитывает кол-во вхождений в строку
     * \param string Исходная строка
     * \param subString Подстрока входящая в строку string
     * \return Кол-во вхождений в строку
     */
    static int count(StringView string, StringView subString);

    /*!
     * \brief Преобразует строку в нижний регистр
     * \warning Только для ANSI-строки
     * \param input Входная строка
     * \return Строку C++
     *
     * \see toUpper()
     */
    static std::string toLower(StringView input);

    /*!
     * \brief Преобразует строку в верхний регистр
     * \warning Только для ANSI-строки
     * \param input Входная строка
     * \return Строку C++
     *
     * \see toLower()
     */
    static std::string toUpper(StringView input);

    /*!
     * \class hide_Trim
     * \note Пробельные символы: <tt>(' ', '\\f', '\\n', '\\r', '\\t', '\\v')</tt>
     * \param input Входная строка
     * \return Строку C++
     */

    /*!
     * \brief Обрезает строку от пробельных символов слева
     *
     * \copydoc hide_Trim
     * \see trimRight(), trim()
     */
    static std::string trimLeft(StringView input);

    /*!
     * \brief Обрезает строку от пробельных символов справа
     *
     * \copydoc hide_Trim
     * \see trimLeft(), trim()
     */
    static std::string trimRight(StringView input);

    /*!
     * \brief Обрезает строку от пробельных символов слева и справа
     *
     * \copydoc hide_Trim
     * \see trimLeft(), trimRight()
     */
    static std::string trim(StringView input);

    static std::string trimLeftIf(StringView input, StringView chars);
    static std::string trimRightIf(StringView input, StringView chars);
    static std::string trimIf(StringView input, StringView chars);

    /// Поведение при заполнении строки символами
    enum Truncate {
        Keep = 0, ///< Не изменять исходную строку
        Trunc = 1 ///< Обрезать строку до указанного кол-ва символов
    };

    static std::string fillLeft(StringView input, size_t width, char fill = ' ', Truncate trunc = Keep);
    static std::string fillRight(StringView input, size_t width, char fill = ' ', Truncate trunc = Keep);

    static void chop(std::string& input, size_t n);
    static std::string capitalize(StringView input);

    /*!
     * \brief Функции не создающие копию строки
     */
    class NonCopy {
    public:
        NonCopy() = delete;

        static void toLower(std::string& input);
        static void toUpper(std::string& input);

        static void trimLeft(std::string& input);
        static void trimRight(std::string& input);
        static void trim(std::string& input);

        static void trimLeftIf(std::string& input, StringView chars);
        static void trimRightIf(std::string& input, StringView chars);
        static void trimIf(std::string& input, StringView chars);

        static void fillLeft(std::string& input, size_t width, char fill = ' ', Truncate trunc = Keep);
        static void fillRight(std::string& input, size_t width, char fill = ' ', Truncate trunc = Keep);

        static void capitalize(std::string& input);
    };

    /*!
     * \brief Функции для работы с UTF-8 строками
     */
    class Utf8 {
    public:
        Utf8() = delete;

        static bool isValid(StringView input);

        static int length(StringView input);
        static void chop(std::string& input, int n);

        static std::wstring toWide(StringView input);
        static std::string toNarrow(WStringView input);
    };

    // Convertions
    // С++17 Замена на to_chars, from_chars
    /// Система счисления
    enum Base {
        Auto = 0, ///< Автоопределение
        Bin = 2,  ///< Двоичная
        Oct = 8,  ///< Восьмеричная
        Dec = 10, ///< Десятичная
        Hex = 16  ///< Шестнадцатеричная
    };

    static int8_t toInt8(StringView input, bool* isOk = nullptr, int base = 10) { return toIntNarrow<int8_t>(input, isOk, base); }
    static int16_t toInt16(StringView input, bool* isOk = nullptr, int base = 10) { return toIntNarrow<int16_t>(input, isOk, base); }
    static int32_t toInt32(StringView input, bool* isOk = nullptr, int base = 10) { return toNumber<int32_t>(input, isOk, base); }
    static int64_t toInt64(StringView input, bool* isOk = nullptr, int base = 10) { return toNumber<int64_t>(input, isOk, base); }

    static uint8_t toUInt8(StringView input, bool* isOk = nullptr, int base = 10) { return toIntNarrow<uint8_t>(input, isOk, base); }
    static uint16_t toUInt16(StringView input, bool* isOk = nullptr, int base = 10) { return toIntNarrow<uint16_t>(input, isOk, base); }
    static uint32_t toUInt32(StringView input, bool* isOk = nullptr, int base = 10) { return toNumber<uint32_t>(input, isOk, base); }
    static uint64_t toUInt64(StringView input, bool* isOk = nullptr, int base = 10) { return toNumber<uint64_t>(input, isOk, base); }

    static float toFloat(StringView input, bool* isOk = nullptr);
    static double toDouble(StringView input, bool* isOk = nullptr);

    static std::string toString(bool value) { return value ? "true" : "false"; }
    static std::string toString(short value, int base = 10) { return toString<short>(value, base); }

    template<class Int, typename std::enable_if<std::is_integral<Int>::value>::type* = nullptr>
    static std::string toString(Int value, int base = 10);

    // FloatFormat
    // 'g' - 1.2345 or 1.e100
    // 'f' - 1.2345
    // 'e' - 1.e100
    static std::string toString(float value, char format = 'g', int precision = 6) { return toString(static_cast<double>(value), format, precision); }
    static std::string toString(double value, char format = 'g', int precision = 6);

private:
    template<class Int>
    static Int toIntNarrow(StringView input, bool* isOk, int base);

    template<class Int>
    static Int toNumber(StringView input, bool* isOk, int base);

    template<class Float>
    static bool checkInfNan(StringView input, bool* isOk, Float& result);
};

#include "StringHelper.tpp"
