#pragma once
#include <vector>
#include <string>

#include "Wrapper/StringView_H.h"

class FileSystemHelper {
public:
    FileSystemHelper() = delete;

    static std::string currentDir();
    static bool makeDir(StringView path);

    enum Filter { All = 0, SkipDots = 1 };
    static std::vector<std::string> entryList(StringView path, Filter filter = All);
    static std::vector<std::string> drives();

    static bool isFile(StringView path);
    static bool isDir(StringView path);

    static bool exists(StringView path);
    static bool rename(StringView oldPath, StringView newPath);

    static bool removeFile(StringView path);
    static bool removeDir(StringView path);

    static std::string tempPath();
    static std::string homePath();
    static std::string rootPath();

    static std::vector<std::string> tempDir(Filter filter = All);
    static std::vector<std::string> homeDir(Filter filter = All);
    static std::vector<std::string> rootDir();

    static std::string cleanPath(StringView path);

    static uint64_t fileSize(StringView path);

private:
    static void normalizeSeparators(std::string& path);
    static std::string normalizePathSegments(StringView name);
    static size_t rootLength(StringView name);
};
