#pragma once
#include <cstdint>

#include <limits>

class XorShift128 {
public:
    typedef uint32_t result_type;

    explicit XorShift128(result_type seed = 123) :
        m_seed(seed) {}

    void seed(result_type value) {
        m_seed = value;
    }

    result_type operator() () {
        result_type s, t = m_seed;
        t ^= t << 11;
        t ^= t >> 8;
        m_seed = c; c = b; b = s = a;
        t ^= s;
        t ^= s >> 19;
        a = t;
        return t;
    }

    static result_type min() { return std::numeric_limits<result_type>::min(); }
    static result_type max() { return std::numeric_limits<result_type>::max(); }

private:
    result_type a = 123456789;
    result_type b = 362436069;
    result_type c = 521288629;
    result_type m_seed;
};
