#pragma once

#include <cstdint>

#include <limits>

class XorShift128Plus {
public:
    typedef uint64_t result_type;

    explicit XorShift128Plus(result_type seed = 123) :
        m_seed(seed) {}

    void seed(result_type value) {
        m_seed = value;
    }

    result_type operator() () {
        result_type x = m_seed;
        result_type const y = a;
        m_seed = y;
        x ^= x << 23;
        a = x ^ y ^ (x >> 17) ^ (y >> 26);
        return a + y;
    }

    static result_type min() { return std::numeric_limits<result_type>::min(); }
    static result_type max() { return std::numeric_limits<result_type>::max(); }

private:
    result_type a = 123456789;
    result_type m_seed = 0;
};
