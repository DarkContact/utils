#pragma once
#include <cstdint>

#include <string>
#include <iosfwd>
#include <tuple>

#include "Wrapper/StringView_H.h"
#include "Utility/TypeTraits.h"
#include "Utility/Macros.h"

class DateTime;

class Date {
    friend class DateTime;

public:
    Date() = default;
    Date(int year, int month, int day);

    bool isValid() const;

    int year() const;
    int month() const;
    int day() const;

    std::string toString(StringView format) const;

    static Date currentDate();

    static bool isValid(int year, int month, int day);
    static bool isLeapYear(int year);

    Date addDays(int days) const;
    Date addMonths(int months) const;
    Date addYears(int years) const;

    int daysInMonth() const;
    int daysInYear() const;

    int dayOfWeek() const;

    friend bool operator== (const Date& lhs, const Date& rhs);
    friend bool operator< (const Date& lhs, const Date& rhs);
    friend std::ostream& operator<<(std::ostream& os, const Date& date);
    friend std::ostream& operator<<(std::ostream& os, const DateTime& date);

private:
    Date(int year, int month, int day, NoValidation_t);
    static void out(std::ostream& os, const Date& date);

    int16_t m_year = 0;
    uint8_t m_month = 0;
    uint8_t m_day = 0;
};


inline bool operator== (const Date& lhs, const Date& rhs) {
    return std::tie(lhs.m_year, lhs.m_month, lhs.m_day) == std::tie(rhs.m_year, rhs.m_month, rhs.m_day);
}
GENERATE_FOR_OPERATOR_EQUAL(Date)

inline bool operator< (const Date& lhs, const Date& rhs) {
    return std::tie(lhs.m_year, lhs.m_month, lhs.m_day) < std::tie(rhs.m_year, rhs.m_month, rhs.m_day);
}
GENERATE_FOR_OPERATOR_LESS(Date)


std::ostream& operator<<(std::ostream& os, const Date& date);
