#pragma once
#include <cstdint>

#include <string>
#include <memory>
#include <limits>
#include <utility>
#include <sstream>

#include "TypeTraits.h"
#include "Helper/StringHelper.h"

/*!
 * \file Output.h
 * \brief Улучшенный вывод, замена для std::cout
 * \details Позволяет выводить в консоль или в файл стандартные типы данных, контейнеры и пользовательские типы данных
 *
 * \par Пример использования
 * \code
 * OUT() << "Hello world!" << 42 << 2.0;    // Utils\Experiment\main.cpp:76 | Hello world! 42 2.0
 * OUT_WARN() << "Hello world!";            // Utils\Experiment\main.cpp:77 | Hello world!
 * OUT_CRIT() << "Hello world!";            // Utils\Experiment\main.cpp:78 | Hello world!
 * std::vector<int> vector = {1, 2, 3, 4};
 * OUT_RELEASE() << STRINGIFY(vector);      // Utils\Experiment\main.cpp:80 | vector [1, 2, 3, 4]
 * \endcode
 *
 * \note Вывод для типов:
 * \code
 * bool             true
 * std::string      "Hello world!"
 * std::nullptr_t   (nullptr)
 * std::pair        (first, second)
 * std::vector      [1, 2, 3]
 *
 * char             'a'
 * char16_t         '\u0061'
 * char32_t         '\u0061' '\U0001f34c'
 *
 * T*               0x79F44C
 * std::unique_ptr  0x79F44C
 * std::shared_ptr  (0x79F44C, used: 2)
 * \endcode
 */

class DebugStream {
    friend class OutputHelper;

private:
    DebugStream(const char* filename, int line, const char* color);

public:
    DebugStream(const DebugStream& copy) = delete;
    DebugStream& operator=(const DebugStream&) = delete;

    ~DebugStream();

    DebugStream& operator<< (std::nullptr_t);
    DebugStream& operator<< (bool value);
    DebugStream& operator<< (short value);
    DebugStream& operator<< (char16_t value);
    DebugStream& operator<< (char32_t value);
    DebugStream& operator<< (wchar_t value);
    DebugStream& operator<< (char value);

    DebugStream& operator<< (char* value);
    DebugStream& operator<< (const char* value);
    DebugStream& operator<< (const std::string& value);

    DebugStream& operator<< (wchar_t* value);
    DebugStream& operator<< (const wchar_t* value);
    DebugStream& operator<< (const std::wstring& value);

    template<typename L, typename R>
    DebugStream& operator<< (const std::pair<L, R>& value) {
        m_buffer += '(';
        *this << value.first;
        m_buffer.pop_back();
        m_buffer += ", ";
        *this << value.second;
        m_buffer.pop_back();
        m_buffer += ") ";
        return *this;
    }

    template<typename Pointer>
    DebugStream& operator<< (Pointer* value) {
        if (value) {
            m_buffer += "0x";
            IntBase prev = m_intBase;
            m_intBase = HexUpper;
            *this << reinterpret_cast<uintptr_t>(value);
            m_intBase = prev;
        } else {
            *this << nullptr;
        }
        return *this;
    }

    template<typename Integer,
             typename std::enable_if<std::is_integral<Integer>::value>::type* = nullptr >
    DebugStream& operator<< (Integer value) {
        int base = 10;
        if (m_intBase == Hex || m_intBase == HexUpper) { base = 16; }
        if (m_intBase == Bin) { base = 2; }
        std::string number = StringHelper::toString(value, base);
        if (m_intBase == HexUpper) {
            StringHelper::NonCopy::toUpper(number);
        }
        if (m_intBase == Bin) {
            size_t width = 64;
            if (value >= 0) {
                auto uInt = ::utils::toUnsigned<Integer>(value);
                if (uInt <= std::numeric_limits<uint8_t>::max()) {
                    width = 8;
                } else if (uInt <= std::numeric_limits<uint16_t>::max()) {
                    width = 16;
                } else if (uInt <= std::numeric_limits<uint32_t>::max()) {
                    width = 32;
                }
                StringHelper::NonCopy::fillLeft(number, width, '0');
            } else {
                StringHelper::NonCopy::fillLeft(number, width, '1');
            }
        }
        m_buffer += number + " ";
        return *this;
    }

    template<typename Enum,
             typename std::enable_if<std::is_enum<Enum>::value>::type* = nullptr >
    DebugStream& operator<< (Enum value) {
        *this << static_cast<typename std::underlying_type<Enum>::type>(value);
        return *this;
    }

    template<typename Floating,
             typename std::enable_if<std::is_floating_point<Floating>::value>::type* = nullptr >
    DebugStream& operator<< (Floating value) {
        m_buffer += StringHelper::toString(value, 'g', std::numeric_limits<Floating>::digits10) + " ";
        return *this;
    }

    template<typename Container,
             typename std::enable_if<isStlContainer<Container>::value>::type* = nullptr >
    DebugStream& operator<< (const Container& container) {
        m_buffer += '[';
        for (auto it = container.cbegin(); it != container.cend(); ++it) {
            *this << *it;
            m_buffer.pop_back();
            if (std::next(it) != container.cend()) {
                m_buffer += ", ";
            }
        }
        m_buffer += "] ";
        return *this;
    }

    template<typename UserData,
             typename std::enable_if<isUserClass<UserData>::value>::type* = nullptr >
    DebugStream& operator<< (const UserData& userData) {
        std::ostringstream stringStream;
        stringStream << userData << ' ';
        m_buffer += stringStream.str();
        return *this;
    }

    //SmartPointers
    template<typename T>
    DebugStream& operator<<(const std::unique_ptr<T>& pointer) {
        *this << pointer.get();
        return *this;
    }

    template<typename T>
    DebugStream& operator<<(const std::shared_ptr<T>& pointer) {
        m_buffer += "(";
        *this << pointer.get();
        m_buffer.pop_back();
        m_buffer += ", used: ";
        *this << pointer.use_count();
        m_buffer.pop_back();
        m_buffer += ") ";
        return *this;
    }

    //Options
    DebugStream& setOutput(std::ostream& ostream, bool onlyText = true);
    DebugStream& noContext();
    DebugStream& noColor();
    DebugStream& onlyText();

    enum IntBase { Decimal = 0, Bin, Hex, HexUpper };
    DebugStream& bin();
    DebugStream& hex();
    DebugStream& hexUpper();

private:
    std::string m_buffer;
    std::string m_context;
    std::ostream* m_ostream;
    bool m_useContext;
    bool m_useColors;
    IntBase m_intBase;
    const char* m_color;
};


class ReleaseStream {
private:
    ReleaseStream() = default;

public:
    ReleaseStream(const ReleaseStream& copy) = delete;
    ReleaseStream& operator=(const ReleaseStream&) = delete;

    template<typename T>
    ReleaseStream& operator<<(const T&) { return *this; }

    ReleaseStream& setOutput(std::ostream&, bool = true) { return *this; }
    ReleaseStream& noContext() { return *this; }
    ReleaseStream& noColor() { return *this; }
    ReleaseStream& onlyText() { return *this; }
    ReleaseStream& bin() { return *this; }
    ReleaseStream& hex() { return *this; }
    ReleaseStream& hexUpper() { return *this; }
};


class OutputHelper {
public:
    OutputHelper(const char* filename, int line, const char* color) :
        m_debugStream(filename, line, color) {}

    DebugStream& debug() { return m_debugStream; }
    static ReleaseStream release() { return {}; }

private:
    DebugStream m_debugStream;
};

#ifdef OUT
 #undef OUT
#endif

#ifdef NDEBUG
 #define OUT OutputHelper::release
 #define OUT_WARN OutputHelper::release
 #define OUT_CRIT OutputHelper::release
#else
 #define OUT OutputHelper(__FILE__, __LINE__, "\033[32m"/*Green*/).debug
 #define OUT_WARN OutputHelper(__FILE__, __LINE__, "\033[33m"/*Yellow*/).debug
 #define OUT_CRIT OutputHelper(__FILE__, __LINE__, "\033[1;31m"/*Bold Red*/).debug
#endif //NDEBUG

#define OUT_RELEASE OutputHelper(__FILE__, __LINE__, "\033[1m"/*Bold White*/).debug

#define STRINGIFY(x) #x << x
