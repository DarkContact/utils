#pragma once
#include <cstdint>

#include <string>
#include <iosfwd>
#include <tuple>

#include "Wrapper/StringView_H.h"
#include "Utility/TypeTraits.h"
#include "Utility/Macros.h"

class DateTime;

class Time {
    friend class DateTime;

public:
    Time() = default;
    Time(int hours, int minutes, int seconds, int milliseconds = 0);

    bool isValid() const;

    int hours() const;
    int minutes() const;
    int seconds() const;
    int milliseconds() const;

    std::string toString(StringView format) const;

    static Time currentTime();

    static bool isValid(int hours, int minutes, int seconds, int milliseconds = 0);

    Time addSecs(int s) const;

    friend bool operator== (const Time& lhs, const Time& rhs);
    friend bool operator< (const Time& lhs, const Time& rhs);
    friend std::ostream& operator<<(std::ostream& os, const Time& time);
    friend std::ostream& operator<<(std::ostream& os, const DateTime& date);

private:
    Time(int hours, int minutes, int seconds, int milliseconds, NoValidation_t);
    void toStringImpl(std::string& result) const;

    int toSecs() const;
    void fromSecs(int totalSecs, int& h, int& m, int& s) const;

    static void out(std::ostream& os, const Time& time);

    uint8_t m_hours = 0;            // [0..23]
    uint8_t m_minutes = 0;          // [0..59]
    uint8_t m_seconds = 0;          // [0..59]
    uint16_t m_milliseconds = 1000; // [0..999]
};


inline bool operator== (const Time& lhs, const Time& rhs) {
    return std::tie(lhs.m_hours, lhs.m_minutes, lhs.m_seconds, lhs.m_milliseconds) == std::tie(rhs.m_hours, rhs.m_minutes, rhs.m_seconds, rhs.m_milliseconds);
}
GENERATE_FOR_OPERATOR_EQUAL(Time)

inline bool operator< (const Time& lhs, const Time& rhs) {
    return std::tie(lhs.m_hours, lhs.m_minutes, lhs.m_seconds, lhs.m_milliseconds) < std::tie(rhs.m_hours, rhs.m_minutes, rhs.m_seconds, rhs.m_milliseconds);
}
GENERATE_FOR_OPERATOR_LESS(Time)

std::ostream& operator<<(std::ostream& os, const Time& time);
