#include "DateTime.h"

#include <cmath>

#include <ostream>
#include <iomanip>

#ifdef _WIN32
 #include <cstring>
 #include "windows.h"
#else
 #include <ctime>
 #include <chrono>
#endif

DateTime::DateTime(const Date& date, const Time& time) :
    m_date(date),
    m_time(time)
{

}

bool DateTime::isValid() const {
    return (m_date.isValid() && m_time.isValid());
}

Date DateTime::date() const {
    return m_date;
}

Time DateTime::time() const {
    return m_time;
}

std::string DateTime::toString(StringView format) const {
    std::string result = m_date.toString(format);
    m_time.toStringImpl(result);
    return result;
}

DateTime DateTime::currentDateTime() {
#ifdef _WIN32
    SYSTEMTIME st;
    std::memset(&st, 0, sizeof(SYSTEMTIME));
    GetLocalTime(&st);
    return DateTime( Date(st.wYear, st.wMonth, st.wDay, noValidation),
                     Time(st.wHour, st.wMinute, st.wSecond, st.wMilliseconds, noValidation) );
#else
    using namespace std::chrono;
    auto time_now = system_clock::now();
    std::time_t time_now_t = system_clock::to_time_t(time_now);

    struct tm* timeInfo = std::localtime(&time_now_t);
    if (timeInfo == nullptr) { return {}; }

    int64_t ns = time_now.time_since_epoch().count() % 1000000000;
    int ms = duration_cast<std::chrono::milliseconds>(nanoseconds(ns)).count();

    return DateTime( Date((timeInfo->tm_year + 1900), (timeInfo->tm_mon + 1), timeInfo->tm_mday, noValidation),
                     Time(timeInfo->tm_hour, timeInfo->tm_min, timeInfo->tm_sec, ms, noValidation) );
#endif
}

DateTime DateTime::addDays(int days) const {
    return DateTime(this->date().addDays(days), this->time());
}

DateTime DateTime::addMonths(int months) const {
    return DateTime(this->date().addMonths(months), this->time());
}

DateTime DateTime::addYears(int years) const {
    return DateTime(this->date().addYears(years), this->time());
}

DateTime DateTime::addSecs(int s) const {
    int secs = this->time().toSecs() + s;
    int days = secs / SECS_PER_DAY;
    if (secs < 0) {
        days = -(std::abs(days) + 1);
    }
    return DateTime(this->date().addDays(days), this->time().addSecs(s));
}

std::ostream& operator<<(std::ostream& os, const DateTime& dateTime) {
    os << "DateTime(";
    Date::out(os, dateTime.date());
    os << " ";
    Time::out(os, dateTime.time());
    os << ")";
    return os;
}

