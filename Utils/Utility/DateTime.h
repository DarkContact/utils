#pragma once
#include "Date.h"
#include "Time.hpp"

class DateTime {
public:
    DateTime() = default;
    DateTime(const Date& date, const Time& time);

    bool isValid() const;

    Date date() const;
    Time time() const;

    std::string toString(StringView format) const;

    static DateTime currentDateTime();

    DateTime addDays(int days) const;
    DateTime addMonths(int months) const;
    DateTime addYears(int years) const;

    DateTime addSecs(int s) const;

    friend bool operator== (const DateTime& lhs, const DateTime& rhs);
    friend bool operator< (const DateTime& lhs, const DateTime& rhs);

private:
    Date m_date;
    Time m_time;
};


inline bool operator== (const DateTime& lhs, const DateTime& rhs) {
    return std::tie(lhs.m_date, lhs.m_time) == std::tie(rhs.m_date, rhs.m_time);
}
GENERATE_FOR_OPERATOR_EQUAL(DateTime)

inline bool operator< (const DateTime& lhs, const DateTime& rhs) {
    return std::tie(lhs.m_date, lhs.m_time) < std::tie(rhs.m_date, rhs.m_time);
}
GENERATE_FOR_OPERATOR_LESS(DateTime)


std::ostream& operator<<(std::ostream& os, const DateTime& dateTime);
