#include "Date.h"

#include <ostream>
#include <iomanip>
#include <array>

#ifdef _WIN32
 #include <cstring>
 #include "windows.h"
#else
 #include <ctime>
 #include <chrono>
#endif

#include "Helper/StringHelper.h"
#include "Utility/DateAlgorithms.h"

Date::Date(int year, int month, int day) :
    m_year(year),
    m_month(month),
    m_day(day)
{
    if (!isValid(m_year, m_month, m_day)) {
        *this = Date();
    }
}

bool Date::isValid() const {
    return (m_day != 0);
}

int Date::year() const {
    return m_year;
}

int Date::month() const {
    return m_month;
}

int Date::day() const {
    return m_day;
}

std::string Date::toString(StringView format) const {
    if (!isValid()) { return ""; }

    //TODO ddd, dddd, MMM, MMMM
    //d - [1, 31]   //dd - [01, 31]
    ///ddd - [Mon, Sun] ///dddd - [Monday, Sunday]
    //M - [1, 12]   //MM - [01, 12]
    ///MMM - [Jan, Dec] ///MMMM - [January, December]
    //yy - [00, 99] //yyyy - [0000, 9999]
    std::string result(format.data(), format.size());

    std::string dayString = StringHelper::toString(m_day);
    StringHelper::replaceAll(result, "dd", StringHelper::fillLeft(dayString, 2, '0'));
    StringHelper::replaceAll(result, "d", dayString);

    std::string monthString = StringHelper::toString(m_month);
    StringHelper::replaceAll(result, "MM", StringHelper::fillLeft(monthString, 2, '0'));
    StringHelper::replaceAll(result, "M", monthString);

    std::string yearString = StringHelper::toString(m_year);
    StringHelper::NonCopy::fillLeft(yearString, 4, '0');
    StringHelper::replaceAll(result, "yyyy", yearString);
    StringHelper::replaceAll(result, "yy", yearString.substr(2, 2));

    return result;
}

//WARNING У реализации через std::localtime, есть проблема 2038 года
Date Date::currentDate() {
#ifdef _WIN32
    SYSTEMTIME st;
    std::memset(&st, 0, sizeof(SYSTEMTIME));
    GetLocalTime(&st);
    return {st.wYear, st.wMonth, st.wDay, noValidation};
#else
    auto time_now = std::chrono::system_clock::now();
    std::time_t time_now_t = std::chrono::system_clock::to_time_t(time_now);

    struct tm* timeInfo = std::localtime(&time_now_t);
    if (timeInfo == nullptr) { return {}; }

    return {(timeInfo->tm_year + 1900), (timeInfo->tm_mon + 1), timeInfo->tm_mday, noValidation};
#endif
}

bool Date::isValid(int year, int month, int day) {
    if (year == 0) { return false; }

    std::array<int, 12> mountDays = {{31, 28, 31, 30, 31, 30,
                                      31, 31, 30, 31, 30, 31}};

    if (month == 2 && Date::isLeapYear(year)) {
        mountDays[1] = 29;
    }

    return ((month >= 1 && month <= 12) &&
            (day >= 1 && day <= mountDays[month - 1]));
}

bool Date::isLeapYear(int year) {
    if (year < 1) {
        ++year;
    }

    return (!(year & 3) && ((year % 25) || !(year & 15)));
}

Date Date::addDays(int days) const {
    if (!isValid()) { return Date(); }
    if (days == 0) { return *this; }

    int64_t civilDays = days_from_civil(static_cast<int64_t>(m_year), m_month, m_day);
    civilDays += days;
    auto civilDate = civil_from_days<int64_t>(civilDays);
    return Date(std::get<0>(civilDate),
                std::get<1>(civilDate),
                std::get<2>(civilDate),
                noValidation);
}

Date Date::addMonths(int months) const {
    if (!isValid()) { return Date(); }
    if (months == 0) { return *this; }

    int old_y = m_year;
    int y = m_year;
    int m = m_month;
    int d = m_day;
    bool increasing = months > 0;

    while (months != 0) {
        if (months < 0 && months + 12 <= 0) {
            y--;
            months += 12;
        } else if (months < 0) {
            m += months;
            months = 0;
            if (m <= 0) {
                --y;
                m += 12;
            }
        } else if (months - 12 >= 0) {
            y++;
            months -= 12;
        } else if (m == 12) {
            y++;
            m = 0;
        } else {
            m += months;
            months = 0;
            if (m > 12) {
                ++y;
                m -= 12;
            }
        }
    }

    // was there a sign change?
    if ((old_y > 0 && y <= 0) ||
        (old_y < 0 && y >= 0))
        // yes, adjust the date by +1 or -1 years
        y += increasing ? +1 : -1;

    if (m == 2) {
        d = std::min(d, isLeapYear(y) ? 29 : 28);
    }

    return {y, m, d, noValidation};
}

Date Date::addYears(int years) const {
    if (!isValid()) { return Date(); }
    if (years == 0) { return *this; }

    int old_y = m_year;
    int y = m_year;
    int m = m_month;
    int d = m_day;
    y += years;

    // was there a sign change?
    if ((old_y > 0 && y <= 0) ||
        (old_y < 0 && y >= 0))
        // yes, adjust the date by +1 or -1 years
        y += years > 0 ? +1 : -1;

    if (m_month == 2) {
        d = std::min(d, isLeapYear(y) ? 29 : 28);
    }

    return {y, m, d, noValidation};
}

int Date::daysInMonth() const {
    if (!isValid()) { return 0; }

    std::array<int, 12> mountDays = {{31, 28, 31, 30, 31, 30,
                                      31, 31, 30, 31, 30, 31}};

    if (m_month == 2 && isLeapYear(m_year)) {
        mountDays[1] = 29;
    }

    return mountDays[m_month - 1];
}

int Date::daysInYear() const {
    if (!isValid()) { return 0; }

    return isLeapYear(m_year) ? 366 : 365;
}

int Date::dayOfWeek() const {
    if (!isValid()) { return 0; }

    int64_t civilDays = days_from_civil(static_cast<int64_t>(m_year), m_month, m_day);
    unsigned weekDay = weekday_from_days(civilDays);
    return weekDay ? weekDay : 7;
}

Date::Date(int year, int month, int day, NoValidation_t) :
    m_year(year),
    m_month(month),
    m_day(day)
{

}

void Date::out(std::ostream& os, const Date& date) {
    os << std::setw(2) << std::setfill('0') << static_cast<int>(date.day()) << "."
       << std::setw(2) << std::setfill('0') << static_cast<int>(date.month()) << "."
       << std::setw(4) << std::setfill('0') << date.year();
}


std::ostream& operator<<(std::ostream& os, const Date& date) {
    os << "Date(";
    Date::out(os, date);
    os << ")";
    return os;
}
