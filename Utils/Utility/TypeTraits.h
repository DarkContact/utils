#pragma once
#include <cstdint>

#include <array>
#include <vector>
#include <deque>
#include <list>
#include <forward_list>
#include <set>
#include <unordered_set>
#include <map>
#include <unordered_map>
#include <stack>
#include <queue>
#include <type_traits>

namespace isStlContainerImpl {
    template <typename T> struct isStlContainer : std::false_type{};
    template <typename T, std::size_t N> struct isStlContainer<std::array    <T,N>>     : std::true_type{};
    template <typename... Args> struct isStlContainer<std::vector            <Args...>> : std::true_type{};
    template <typename... Args> struct isStlContainer<std::deque             <Args...>> : std::true_type{};
    template <typename... Args> struct isStlContainer<std::list              <Args...>> : std::true_type{};
    template <typename... Args> struct isStlContainer<std::forward_list      <Args...>> : std::true_type{};
    template <typename... Args> struct isStlContainer<std::set               <Args...>> : std::true_type{};
    template <typename... Args> struct isStlContainer<std::multiset          <Args...>> : std::true_type{};
    template <typename... Args> struct isStlContainer<std::unordered_set     <Args...>> : std::true_type{};
    template <typename... Args> struct isStlContainer<std::unordered_multiset<Args...>> : std::true_type{};
    template <typename... Args> struct isStlContainer<std::map               <Args...>> : std::true_type{};
    template <typename... Args> struct isStlContainer<std::multimap          <Args...>> : std::true_type{};
    template <typename... Args> struct isStlContainer<std::unordered_map     <Args...>> : std::true_type{};
    template <typename... Args> struct isStlContainer<std::unordered_multimap<Args...>> : std::true_type{};
    template <typename... Args> struct isStlContainer<std::stack             <Args...>> : std::true_type{};
    template <typename... Args> struct isStlContainer<std::queue             <Args...>> : std::true_type{};
    template <typename... Args> struct isStlContainer<std::priority_queue    <Args...>> : std::true_type{};
}

template <typename T>
using isStlContainer = std::integral_constant<bool, isStlContainerImpl::isStlContainer<typename std::decay<T>::type>::value>;

template<typename T>
using isUserClass = std::integral_constant<bool, std::is_class<T>::value && !isStlContainer<T>::value>;

template<typename T>
using isFloatingWithoutLongDouble = std::integral_constant<bool, std::is_floating_point<T>::value && !std::is_same<long double, T>::value>;

namespace utils {
    template <typename T>
    struct MakeUnsigned { typedef T Type; };

    #define TO_UNSIGNED(sType, uType) \
    template <>	\
    struct MakeUnsigned<sType> { typedef uType Type; };

    TO_UNSIGNED(char, unsigned char)
    TO_UNSIGNED(signed char, unsigned char)
    TO_UNSIGNED(short, unsigned short)
    TO_UNSIGNED(int, unsigned int)
    TO_UNSIGNED(long, unsigned long)
    TO_UNSIGNED(long long, unsigned long long)
    #undef TO_UNSIGNED

    template <typename Int>
    inline typename MakeUnsigned<Int>::Type toUnsigned(Int value) {
        return static_cast<typename MakeUnsigned<Int>::Type>(value);
    }
}

struct NoValidation_t { };
constexpr NoValidation_t noValidation { };
const int SECS_PER_DAY = 86400;
