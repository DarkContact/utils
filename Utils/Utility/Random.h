#pragma once
#include <cstdint>
#include <cassert>

#include <chrono>
#include <random>

#include "Helper/Generators/XorShift128.h"
#include "Helper/Generators/XorShift128Plus.h"

class Random {
public:
    Random() = delete;

    static void setSeed() {
        getGenerator<XorShift128>().seed(static_cast<XorShift128::result_type>(getSeed()));
        getGenerator<XorShift128Plus>().seed(static_cast<XorShift128Plus::result_type>(getSeed()));
    }

    static void setSeed(int64_t seed) {
        getGenerator<XorShift128>().seed(static_cast<XorShift128::result_type>(seed));
        getGenerator<XorShift128Plus>().seed(static_cast<XorShift128Plus::result_type>(seed));
    }

    static int8_t int8() { return random<int8_t, XorShift128>(std::numeric_limits<int8_t>::min(), std::numeric_limits<int8_t>::max()); }
    static int16_t int16() { return random<int16_t, XorShift128>(std::numeric_limits<int16_t>::min(), std::numeric_limits<int16_t>::max()); }
    static int32_t int32() { return random<int32_t, XorShift128>(std::numeric_limits<int32_t>::min(), std::numeric_limits<int32_t>::max()); }
    static int64_t int64() { return random<int64_t, XorShift128Plus>(std::numeric_limits<int64_t>::min(), std::numeric_limits<int64_t>::max()); }

    static uint8_t uInt8() { return random<uint8_t, XorShift128>(std::numeric_limits<uint8_t>::min(), std::numeric_limits<uint8_t>::max()); }
    static uint16_t uInt16() { return random<uint16_t, XorShift128>(std::numeric_limits<uint16_t>::min(), std::numeric_limits<uint16_t>::max()); }
    static uint32_t uInt32() { return random<uint32_t, XorShift128>(std::numeric_limits<uint32_t>::min(), std::numeric_limits<uint32_t>::max()); }
    static uint64_t uInt64() { return random<uint64_t, XorShift128Plus>(std::numeric_limits<uint64_t>::min(), std::numeric_limits<uint64_t>::max()); }

    static float toFloat() { return randomFloat<float, XorShift128>(0.0f, 1.0f); }
    static double toDouble() { return randomFloat<double, XorShift128>(0.0, 1.0); }

    static int8_t int8(int8_t min, int8_t max) { return random<int8_t, XorShift128>(min, max); }
    static int16_t int16(int16_t min, int16_t max) { return random<int16_t, XorShift128>(min, max); }
    static int32_t int32(int32_t min, int32_t max) { return random<int32_t, XorShift128>(min, max); }
    static int64_t int64(int64_t min, int64_t max) { return random<int64_t, XorShift128Plus>(min, max); }

    static uint8_t uInt8(uint8_t min, uint8_t max) { return random<uint8_t, XorShift128>(min, max); }
    static uint16_t uInt16(uint16_t min, uint16_t max) { return random<uint16_t, XorShift128>(min, max); }
    static uint32_t uInt32(uint32_t min, uint32_t max) { return random<uint32_t, XorShift128>(min, max); }
    static uint64_t uInt64(uint64_t min, uint64_t max) { return random<uint64_t, XorShift128Plus>(min, max); }

    static float toFloat(float min, float max) { return randomFloat<float, XorShift128>(min, max); }
    static double toDouble(double min, double max) { return randomFloat<double, XorShift128>(min, max); }

private:
    static int64_t getSeed() {
        return std::chrono::system_clock::now().time_since_epoch().count();
    }

    template<class Generator>
    static Generator& getGenerator() {
        thread_local Generator gen{ static_cast<typename Generator::result_type>(getSeed()) };
        return gen;
    }

    template<class Int, class Generator>
    static Int random(Int min, Int max) {
        static_assert(std::is_integral<Int>(), "Type not an integral!");
        assert(min < max);

        auto& gen = getGenerator<Generator>();

        std::uniform_int_distribution<Int> dist(min, max);
        return dist(gen);
    }

    template<class Float, class Generator>
    static Float randomFloat(Float min, Float max) {
        static_assert(std::is_floating_point<Float>(), "Type not an integral!");
        assert(min < max);

        auto& gen = getGenerator<Generator>();

        std::uniform_real_distribution<Float> dist(min, max);
        return dist(gen);
    }
};
