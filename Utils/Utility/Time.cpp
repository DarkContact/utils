#include "Time.hpp"

#include <ostream>
#include <iomanip>

#ifdef _WIN32
 #include <cstring>
 #include "windows.h"
#else
 #include <ctime>
 #include <chrono>
#endif

#include "Helper/StringHelper.h"

Time::Time(int hours, int minutes, int seconds, int milliseconds) :
    m_hours(hours),
    m_minutes(minutes),
    m_seconds(seconds),
    m_milliseconds(milliseconds)
{
    if (!isValid(m_hours, m_minutes, m_seconds, m_milliseconds)) {
        *this = Time();
    }
}

bool Time::isValid() const {
    return (m_milliseconds != 1000);
}

int Time::hours() const {
    return m_hours;
}

int Time::minutes() const {
    return m_minutes;
}

int Time::seconds() const {
    return m_seconds;
}

int Time::milliseconds() const {
    return m_milliseconds;
}

std::string Time::toString(StringView format) const {
    std::string result(format.data(), format.size());
    toStringImpl(result);
    return result;
}

Time Time::currentTime() {
#ifdef _WIN32
    SYSTEMTIME st;
    std::memset(&st, 0, sizeof(SYSTEMTIME));
    GetLocalTime(&st);
    return {st.wHour, st.wMinute, st.wSecond, st.wMilliseconds, noValidation};
#else
    using namespace std::chrono;
    auto time_now = system_clock::now();
    std::time_t time_now_t = system_clock::to_time_t(time_now);

    struct tm* timeInfo = std::localtime(&time_now_t);
    if (timeInfo == nullptr) { return {}; }

    int64_t ns = time_now.time_since_epoch().count() % 1000000000;
    int ms = duration_cast<std::chrono::milliseconds>(nanoseconds(ns)).count();

    return {timeInfo->tm_hour, timeInfo->tm_min, timeInfo->tm_sec, ms, noValidation};
#endif
}

bool Time::isValid(int hours, int minutes, int seconds, int milliseconds) {
    return ( (hours >= 0 && hours < 24)
             && (minutes >= 0 && minutes < 60)
             && (seconds >= 0 && seconds < 60)
             && (milliseconds >= 0 && milliseconds < 1000) );
}

Time Time::addSecs(int s) const {
    int secs = toSecs() + s;
    secs %= SECS_PER_DAY;
    if (secs < 0) {
        secs = SECS_PER_DAY + secs;
    }
    int hh, mm, ss;
    fromSecs(secs, hh, mm, ss);
    return {hh, mm, ss, m_milliseconds};
}

Time::Time(int hours, int minutes, int seconds, int milliseconds, NoValidation_t) :
    m_hours(hours),
    m_minutes(minutes),
    m_seconds(seconds),
    m_milliseconds(milliseconds)
{

}

void Time::toStringImpl(std::string& result) const {
    if (!isValid()) { result.clear(); return; }

    //H - [0, 23]   //HH - [00, 23]
    //m - [0, 59]   //mm - [00, 59]
    //s - [0, 59]   //ss - [00, 59]
    //z - [0, 999]  //zzz - [000, 999]
    std::string hoursString = StringHelper::toString(m_hours);
    StringHelper::replaceAll(result, "HH", StringHelper::fillLeft(hoursString, 2, '0'));
    StringHelper::replaceAll(result, "H", hoursString);

    std::string minuteString = StringHelper::toString(m_minutes);
    StringHelper::replaceAll(result, "mm", StringHelper::fillLeft(minuteString, 2, '0'));
    StringHelper::replaceAll(result, "m", minuteString);

    std::string secondString = StringHelper::toString(m_seconds);
    StringHelper::replaceAll(result, "ss", StringHelper::fillLeft(secondString, 2, '0'));
    StringHelper::replaceAll(result, "s", secondString);

    std::string millisecondString = StringHelper::toString(m_milliseconds);
    StringHelper::replaceAll(result, "zzz", StringHelper::fillLeft(millisecondString, 3, '0'));
    StringHelper::replaceAll(result, "z", millisecondString);
}

int Time::toSecs() const {
    return (m_hours * 3600) + (m_minutes * 60) + m_seconds;
}

void Time::fromSecs(int totalSecs, int& h, int& m, int& s) const {
    h = totalSecs / 3600;
    totalSecs -= (h * 3600);
    m = totalSecs / 60;
    totalSecs -= (m * 60);
    s = totalSecs;
}

void Time::out(std::ostream& os, const Time& time) {
    os << std::setw(2) << std::setfill('0') << time.hours() << ":"
       << std::setw(2) << std::setfill('0') << time.minutes() << ":"
       << std::setw(2) << std::setfill('0') << time.seconds() << "."
       << std::setw(3) << std::setfill('0') << time.milliseconds();
}

std::ostream& operator<<(std::ostream& os, const Time& time) {
    os << "Time(";
    Time::out(os, time);
    os << ")";
    return os;
}
