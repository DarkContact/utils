#pragma once

/*!
 * \defgroup macros Макросы
 * Вспомогательные макросы
 */

/*!
 * \ingroup macros
 * \def GENERATE_FOR_OPERATOR_EQUAL(Type)
 * \brief Создаёт необходимые дополнительные перегрузки для оператора ==
 */
#define GENERATE_FOR_OPERATOR_EQUAL(Type) \
    inline bool operator!= (const Type& lhs, const Type& rhs) { return !(lhs == rhs); }

/*!
 * \ingroup macros
 * \def GENERATE_FOR_OPERATOR_LESS(Type)
 * \brief Создаёт необходимые дополнительные перегрузки для оператора <
 */
#define GENERATE_FOR_OPERATOR_LESS(Type) \
    inline bool operator> (const Type& lhs, const Type& rhs) { return rhs < lhs; } \
    inline bool operator<= (const Type& lhs, const Type& rhs) { return !(lhs > rhs); } \
    inline bool operator>= (const Type& lhs, const Type& rhs) { return !(lhs < rhs); }

#define DELELE_COPY_AND_ASSIGN(Type) \
    Type(const Type&) = delete; \
    Type& operator=(const Type&) = delete;

#define DEFAULT_MOVE_AND_ASSIGN(Type) \
    Type(Type&&) = default; \
    Type& operator= (Type&&) = default;

#define DELETE_MOVE_AND_ASSIGN(Type) \
    Type(Type&&) = delete; \
    Type& operator= (Type&&) = delete;

#define SINGLETON(Type) \
    static Type& instance() { \
        static Type instance; \
        return instance; \
    } \
    DELELE_COPY_AND_ASSIGN(Type) \
    DELETE_MOVE_AND_ASSIGN(Type)


//Compiler specific

//MinGW:
#define LIKELY(x)               __builtin_expect((x), 1)
#define UNLIKELY(x)             __builtin_expect((x), 0)
#define COUNT_LEAD_ZERO_64(x)   __builtin_clzll((x))


//Doxygen Documentation For Directory

/*!
 * \dir ../Wrapper
 * \brief Обёртки для подключения библиотек или возможностей новых стандартов C++
 */

/*!
 * \dir ../Helper
 * \brief Вспомогательные классы
 */
