#include "Output.h"

#include <iostream>

DebugStream::DebugStream(const char* filename, int line, const char* color) :
    m_buffer(),
    m_context(),
    m_ostream(&std::cout),
    m_useContext(true),
    m_useColors(true),
    m_intBase(Decimal),
    m_color(color)
{
    m_context += filename;
    StringHelper::replaceAll(m_context, "..\\", "");
    m_context += ":";
    m_context += StringHelper::toString(line);
    m_context += " | ";
}

DebugStream::~DebugStream() {
    const char* grayColor = "\033[37m";
    const char* endColor = "\033[0m";

    if (!m_buffer.empty()) {
        if (m_useContext) {
            if (m_useColors) { *m_ostream << grayColor; }
            *m_ostream << m_context;
            if (m_useColors) { *m_ostream << endColor; }
        }

        if (m_useColors) { *m_ostream << m_color; }
        m_buffer.pop_back();
        *m_ostream << m_buffer;
        if (m_useColors) { *m_ostream << endColor; }

        *m_ostream << std::endl;
    }
}

DebugStream& DebugStream::operator<<(short value) {
    *this << static_cast<int>(value);
    return *this;
}

DebugStream& DebugStream::operator<<(std::nullptr_t) {
    m_buffer += "(nullptr) ";
    return *this;
}

DebugStream& DebugStream::operator<<(bool value) {
    m_buffer += value ? "true " : "false ";
    return *this;
}

DebugStream& DebugStream::operator<<(char16_t value) {
    m_buffer += "'\\u";
    std::string result = StringHelper::toString(static_cast<uint16_t>(value), 16);
    StringHelper::NonCopy::fillLeft(result, 4, '0');
    m_buffer += result;
    m_buffer += "' ";
    return *this;
}

DebugStream& DebugStream::operator<<(char32_t value) {
    if (value <= 0xffff) {
        *this << static_cast<char16_t>(value);
        return *this;
    }

    m_buffer += "'\\U";
    std::string result = StringHelper::toString(static_cast<uint32_t>(value), 16);
    StringHelper::NonCopy::fillLeft(result, 8, '0');
    m_buffer += result;
    m_buffer += "' ";
    return *this;
}

DebugStream& DebugStream::operator<<(wchar_t value) {
    *this << static_cast<char32_t>(value);
    return *this;
}

DebugStream& DebugStream::operator<<(char value) {
    m_buffer += "\'";
    m_buffer += value;
    m_buffer += "\' ";
    return *this;
}

DebugStream& DebugStream::operator<<(char* value) {
    *this << const_cast<const char*>(value);
    return *this;
}

DebugStream& DebugStream::operator<<(const char* value) {
    if (value) {
        m_buffer += value;
        m_buffer += ' ';
    } else {
        *this << nullptr;
    }
    return *this;
}

DebugStream& DebugStream::operator<<(const std::string& value) {
    m_buffer += '\"';
    m_buffer += value;
    m_buffer += "\" ";
    return *this;
}

DebugStream& DebugStream::operator<<(wchar_t* value) {
    *this << const_cast<const wchar_t*>(value);
    return *this;
}

DebugStream& DebugStream::operator<<(const wchar_t* value) {
    if (value) {
        m_buffer += StringHelper::Utf8::toNarrow(value);
        m_buffer += ' ';
    } else {
        *this << nullptr;
    }
    return *this;
}

DebugStream& DebugStream::operator<<(const std::wstring& value) {
    *this << StringHelper::Utf8::toNarrow(value);
    return *this;
}

DebugStream& DebugStream::setOutput(std::ostream& ostream, bool onlyText) {
    if (onlyText) {
        DebugStream::onlyText();
    }
    m_ostream = &ostream;
    return *this;
}

DebugStream& DebugStream::noContext() {
    m_useContext = false;
    return *this;
}

DebugStream& DebugStream::noColor() {
    m_useColors = false;
    return *this;
}

DebugStream& DebugStream::onlyText() {
    noColor();
    noContext();
    return *this;
}

DebugStream& DebugStream::bin() {
    m_intBase = Bin;
    return *this;
}

DebugStream& DebugStream::hex() {
    m_intBase = Hex;
    return *this;
}

DebugStream& DebugStream::hexUpper() {
    m_intBase = HexUpper;
    return *this;
}
