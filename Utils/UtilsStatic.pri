INCLUDEPATH += $$PWD
LIBS += -L$$PWD/build -lUtils

# MinGW or GCC
win32-g++|unix {
    POST_TARGETDEPS += $$PWD/build/libUtils.a
}
