#pragma once
#include <ostream>

#include <optional>

template<class T>
using Optional = std::optional<T>;
constexpr auto nullopt = std::nullopt;

template<class T>
std::ostream& operator<<(std::ostream& os, const Optional<T>& optional) {
    if (optional) {
        os << "(value: " << optional.value() << ")";
    } else {
        os << "(nullopt)";
    }
    return os;
}
