#pragma once

#define HAVE_STRING_VIEW

#ifdef HAVE_STRING_VIEW
 #include <string>
 #include <string_view>
 using StringView = std::string_view;
 using WStringView = std::wstring_view;
#else
 #include <string>
 using StringView = const std::string&;
 using WStringView = const std::wstring&;
#endif

namespace utils {
    inline std::string toString(StringView input) {
        return {input.data(), input.size()};
    }

    inline std::wstring toWString(WStringView input) {
        return {input.data(), input.size()};
    }
}
