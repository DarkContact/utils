# Utils
Библиотека утилит для расширения возможностей стандартного C++

# Зависимости:
* [double-conversion](https://github.com/google/double-conversion) - Efficient binary-decimal and decimal-binary conversion routines for IEEE doubles
* [utfcpp](https://github.com/nemtrif/utfcpp) - UTF-8 with C++ in a Portable Way

# Namespaces:
* utils
* details (всё скрытое от клиентского кода)

# TODO:

## November:
- [x] Пересмотреть наименование файлов в проекте Benchmark и Tests
- [x] Выделить charconv и cctype из StdReplacer
- [x] Настройки проектов QMAKE_CXXFLAGS и прочие
- [x] UtilsDir POST_TARGETDEPS Отдельный файл для включения
- [x] Документировать вывод класса Output

## December:
- [x] StringHelper, перенести double-conversion в .cpp файл, чтобы избавиться от зависимости

- [ ] Уход от статических классов в пользу namespace
    - [ ] StringHelper -> utils_str
    - [ ] FileSystemHelper -> utils_fs
    - [ ] Random -> utils

- [ ] to_chars to_chars_result -> Переименовать toChars toCharsResult
- [ ] NonCopy методы заменить на Copy по аналогии с Boost
- [ ] Создать метод format, по аналогии с библиотекой fmt или printf

- [ ] Актуализировать charconv для стандарта C++17 (constexpr if)
- [ ] Методы для работы с диапазонами (Например: char* start, char* end)
- [ ] Доработка trimLeftIf, trimRightIf, trimIf (изменить сигнатуру на предикаты)

# MAYBE:
- [ ] ? Переименовать .h в .hpp
- [ ] ? Пересмотреть наименование Helpers -> ClassHelper
- [ ] ? Продумать общую структуру директорий и именований


# Benchmarks:

MinGW 7.3.0 64bit (c++17) <br/>
Qt 5.12.0-beta-4 <br/>
Run on (8 X 3492 MHz CPU s)

| Functions          | Utils  | Qt      | std    |
| ------------------ | ------ | ------- | ------ |
| `startWith`        | 8 ns   | 10 ns   | -      |
| `endsWith`         | 5 ns   | 11 ns   | -      |
| `contains`         | 2 ns   | 23 ns   | -      |
| `replaceAll`       | 20 ns  | 63 ns   | -      |
| `split`            | 523 ns | 1943 ns | -      |
| `join`             | 256 ns | 389 ns  | -      |
| `count (ch)`       | 14 ns  | 15 ns   | -      |
| `count (substing)` | 21 ns  | 101 ns  | -      |
| `toLower`          | 24 ns  | 117 ns  | 61 ns  |
| `toUpper`          | 23 ns  | 110 ns  | 47 ns  |
| `trimLeft`         | 53 ns  | -       | -      |
| `trimRight`        | 53 ns  | -       | -      |
| `trim`             | 26 ns  | 17 ns   | -      |
| `fillLeft`         | 80 ns  | 86 ns   | -      |
| `fillRight`        | 78 ns  | 90 ns   | -      |
| `chop`             | 2 ns   | 5 ns    | -      |


MinGW 7.3.0 64bit (c++17) <br/>
Qt 5.12.0-beta-4 <br/>
Run on (4 X 2712 MHz CPU s)

| Functions          | Utils  | Qt      | std    |
| ------------------ | ------ | ------- | ------ |
| `startWith`        | 5 ns   | 9 ns    | -      |
| `endsWith`         | 3 ns   | 9 ns    | -      |
| `contains`         | 2 ns   | 19 ns   | -      |
| `replaceAll`       | 17 ns  | 60 ns   | -      |
| `split`            | 488 ns | 3209 ns | -      |
| `join`             | 255 ns | 360 ns  | -      |
| `count (ch)`       | 10 ns  | 10 ns   | -      |
| `count (substing)` | 17 ns  | 113 ns  | -      |
| `toLower`          | 21 ns  | 134 ns  | 32 ns  |
| `toUpper`          | 21 ns  | 126 ns  | 29 ns  |
| `trimLeft`         | 67 ns  | -       | -      |
| `trimRight`        | 67 ns  | -       | -      |
| `trim`             | 22 ns  | 15 ns   | -      |
| `fillLeft`         | 80 ns  | 93 ns   | -      |
| `fillRight`        | 80 ns  | 93 ns   | -      |
| `chop`             | 2 ns   | 4 ns    | -      |
