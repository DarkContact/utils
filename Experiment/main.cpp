#include "Helper/StringHelper.h"
#include "Helper/FileSystemHelper.h"

#include "Utility/Output.h"
#include "Utility/Date.h"
#include "Utility/Time.hpp"
#include "Utility/DateTime.h"
#include "Utility/Random.h"

#include <cfloat>

#include <fstream>
#include <iostream>
#include <iomanip>
#include <limits>
#include <memory>
#include <map>

#include <QString>
#include <QDateTime>
#include <QDir>
#include <QDebug>

#include <Standart/CharConversions.h>
#include <Standart/CharType.h>

int main() {
//    OUT_RELEASE() << FileSystemHelper::fileSize("D:/Тест/01.txt");
//    OUT_RELEASE() << FileSystemHelper::fileSize("D:/Тест/02.rtf");
//    OUT_RELEASE() << FileSystemHelper::fileSize("D:/Тест/03.wtf");

    qDebug() << QString("Test string %1, %2, %3").arg(12).arg(1.2567e-30, 6, 'f').arg(true);
    qDebug() << QString("Test string %1").arg(1.25);
    qDebug() << QString::number(1.25, 'g', 4);
    qDebug() << QString::number(1.25, 'g', 1);
    qDebug() << QString::number(1.25, 'g', 2);
    qDebug() << QString::number(1.25, 'g', 3);

    qDebug() << QString::number(1.25, 'f', 6);

    qDebug() << QString::number(12345.12312345);

    qDebug() << QString::number(-0.10e-100, 'e', 2);
    qDebug() << QString::number(0.10e-100, 'e', 2);

    qDebug() << QString::number(-0.10e-100, 'g', 2);
    qDebug() << QString::number(0.10e-100, 'g', 2);

    qDebug() << QString::number(-0.10e-100, 'G', 2);
    qDebug() << QString::number(0.10e-100, 'G', 2);

    qDebug() << QString::number(-0.10e-1, 'g', 2);
    qDebug() << QString::number(-0.10e-2, 'g', 2);
    qDebug() << QString::number(-0.10e-4, 'g', 2);
    qDebug() << QString::number(-0.10e-5, 'g', 2);

    qDebug() << 1.5f;
    qDebug() << 1.5245;

    qDebug() << std::numeric_limits<double>::infinity();
    qDebug() << -std::numeric_limits<double>::infinity();
    qDebug() << QString::number(std::numeric_limits<double>::infinity(), 'G');

    qDebug() << std::numeric_limits<double>::quiet_NaN();
    qDebug() << -std::numeric_limits<double>::quiet_NaN();
    qDebug() << QString::number(std::numeric_limits<double>::quiet_NaN(), 'G');

    qDebug() << QString::number(-0.10e-100, 'g', 0);
    qDebug() << QString::number(0.10e-100, 'g', 0);

    std::fprintf(stderr, "%g\n", 12345.12312345);
    std::fprintf(stderr, "%.2g\n", 0.10e-100);
    std::fprintf(stderr, "%.2g\n", -0.10e-5);

//    std::string input = "9875612345abcdefg56789";
//    StringHelper::NonCopy::trimLeftIf(input, [](int ch) { return ((ch >= '1' && ch <= '9') || ch == '0');} );
//    OUT_RELEASE() << input;

    static std::string str = "ABC123DeF456abc";
    for (auto& ch : str) {
        ch = std::tolower(ch);
    }
    OUT_RELEASE() << str;

    std::ofstream oFile("Experiment.txt");
    std::string utfReplace = "Привет ми мир мир!";
    StringHelper::replaceAll(utfReplace, "ми", "авг");
    OUT_RELEASE().setOutput(oFile) << utfReplace;

    return EXIT_SUCCESS;
}
