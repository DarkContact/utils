TEMPLATE = app
CONFIG += windows
CONFIG += qt

include(../Utils/UtilsStatic.pri)

include(../../External/Configuration.pri)

INCLUDEPATH += ../Utils

SOURCES += \
    main.cpp
